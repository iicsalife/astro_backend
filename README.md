#Project Description

###api
``` bash
! Create a file .env and put your configuration

# Composer 
$ composer install 

# Generate key
$ php artisan key:generate

# Creating bases
$ php artisan migrate

# Adding important information
$ php artisan db:seed

# start
$ php artisan serve
```
###web
``` bash
! Create a file nuxt.config.js and put your configuration

# Install dependencies
$ npm install

# Serve with hot reload at localhost:3000
$ npm run dev

# Build for production and launch server
$ npm run build
$ npm start

# Generate static project
$ npm run generate
```