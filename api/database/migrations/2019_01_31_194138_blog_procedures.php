<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class BlogProcedures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE PROCEDURE `pr_add_article` (IN `p_author_id` INT, IN `p_category_id` INT, IN `p_slug` VARCHAR(255),IN `p_title` VARCHAR(255), IN `p_content` TEXT, IN `p_photo` VARCHAR(255))  BEGIN
	insert into articles 
    (user_id, category_id,slug,title,content,photo)
    VALUES(p_author_id,p_category_id, p_slug,p_title,p_content,p_photo);
    update users
    set publications = publications+1
    WHERE id = p_author_id;
    select LAST_INSERT_ID() as article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_add_article_comment` (IN `p_article_id` INT, IN `p_comment_author` INT, IN `p_comment_body` TEXT, IN `p_parent_id` INT)  BEGIN
	insert into article_comments 
    (article_id, comment_author, body, parent_id)
    VALUES(p_article_id, p_comment_author, p_comment_body, p_parent_id);
    UPDATE articles
    set comment_cnt = comment_cnt+1
    WHERE id = p_article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_add_tag_article` (IN `p_article_id` INT, IN `p_tag_id` INT)  BEGIN
	insert into article_tags
    (article_id,tags_id)
    VALUES(p_article_id,p_tag_id);
END');
        DB::unprepared('CREATE PROCEDURE `pr_dis_like_article` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
	delete from article_likes
	where article_id = p_article_id and(user_id = p_user_id);
    UPDATE articles
    set is_like = is_like-1
    WHERE id = p_article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_get_article` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
    declare path nvarchar(30)	 default fu_get_sys_param (\'AUTHOR_PHOTO_PATH\');
    UPDATE articles
    set views_cnt = views_cnt+1
    WHERE id = p_article_id;
	select
		a.id,
		u.id as author,
        c.name as category_name,
        a.slug,
		a.category_id,
		a.title as article_title,
		a.content as article_content,
		a.views_cnt,
		a.is_like as likes_cnt,
		a.is_save ,
		a.created_at,
        a.comment_cnt as article_comment,
        concat(path,u.avatar) as autor_photo,
        a.photo as article_photo,
        group_concat(atg.tags_id) as tag_list,
        (select article_id from article_likes where article_id = p_article_id and(user_id = p_user_id)) as user_is_like
	from articles a
	left join users u on a.user_id = u.id
    left join categories c on a.category_id = c.id
	left join article_tags atg on a.id = atg.article_id
	where a.id = p_article_id
	group by a.id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_get_article_comment` (IN `p_article_id` INT)  BEGIN
	declare path nvarchar(30)	 default fu_get_sys_param (\'AUTHOR_PHOTO_PATH\');
	select
		ac.body,
        ac.parent_id as parent_comment_id,
		u.name as comment_autor,
        concat(path,u.avatar) as autor_photo
	from article_comments ac
	left join users u on ac.comment_author = u.id
	where ac.article_id = p_article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_get_article_list` (IN `p_category_id` INT, IN `p_tag_list` VARCHAR(255), IN `p_page_num` INT)  BEGIN
	declare l_per_page 		int default fu_get_sys_param (\'FORUM_ARTICLE_LIST_CNT\');
    declare path nvarchar(30)	 default fu_get_sys_param (\'AUTHOR_PHOTO_PATH\');
	declare l_offset 			int default (p_page_num * l_per_page);
	select
		a.id,
		u.name as author_name,
        c.name as category_name,
        a.slug,
		a.category_id,
        a.comment_cnt,
		a.title as aricle_title,
		a.content as article_content,
		a.views_cnt,
		a.is_like as likes_cnt,
		a.is_save,
		a.created_at,
        concat(path,u.avatar) as autor_photo,
        group_concat(atg.tags_id) as tag_list
	from articles a
	left join users u on a.user_id = u.id
    left join categories c on a.category_id = c.id
	left join article_tags atg on a.id = atg.article_id
	where a.category_id = p_category_id
 	and (p_tag_list is null or p_tag_list = \'0\' or FIND_IN_SET(atg.tags_id, p_tag_list))
    group by a.id
	limit l_offset, l_per_page;
END');
        DB::unprepared('CREATE PROCEDURE `pr_get_author` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
    declare path nvarchar(30)	 default fu_get_sys_param (\'AUTHOR_PHOTO_PATH\');
	select
		u.name as author_name,
		u.description as author_description,
        u.followers as author_follow_cnt,
        u.likes as author_like_cnt,
        u.publications as author_public_cnt,
        (select user_id from author_follow where user_id = p_user_id and(author_id = p_author_id)) as user_is_follow,
        concat(path,u.avatar) as autor_avatar
	from users u
	where u.id = p_author_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_set_article_status` (IN `p_article_id` INT, IN `p_is_active` INT)  BEGIN
	update articles
	set 
			is_active = p_is_active
	where id = p_article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_set_follower` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
	insert into author_follow 
    (author_id, user_id)
    VALUES(p_author_id, p_user_id);
    UPDATE users
    set followers = followers+1
    WHERE id = p_author_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_del_follower` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
	delete from author_follow 
	where author_id = p_author_id and(user_id = p_user_id);
    UPDATE users
    set followers = followers-1
    WHERE id = p_author_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_set_article_like` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
	insert into article_likes 
    (article_id, user_id)
    VALUES(p_article_id, p_user_id);
    UPDATE articles
    set is_like = is_like+1
    WHERE id = p_article_id;
END');
        DB::unprepared('CREATE PROCEDURE `pr_update_article` (IN `p_article_id` INT, IN `p_category_id` INT, IN `p_author_id` INT, IN `p_title` VARCHAR(255), IN `p_content` TEXT, IN `p_photo` VARCHAR(255))  BEGIN
	update articles
	set 
			category_id = p_category_id,
			title = p_title,
			content = p_content,
			photo = p_photo,
            user_id = p_author_id
	where id = p_article_id;
END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_add_article');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_add_article_comment');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_dis_like_article');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_add_tag_article');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_article');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_article_comment');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_article_list');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_author');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_set_article_status');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_set_follower');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_del_follower');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_set_article_like');
        DB::unprepared('DROP PROCEDURE IF EXISTS pr_update_article');
    }
}
