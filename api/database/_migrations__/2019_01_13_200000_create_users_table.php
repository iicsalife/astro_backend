<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('role_id');
			$table->string('name');
			$table->string('email')->unique();
			$table->timestamp('email_verified_at')->nullable();
			$table->unsignedInteger('position_id');
			$table->tinyInteger('status');
			$table->string('avatar');
			$table->string('password');
			$table->rememberToken();

			$table->text('description');
			$table->integer('followers')->default(0);
			$table->integer('likes')->default(0);
			$table->integer('publications')->default(0);
			$table->timestamps();

			$table->foreign('role_id')
				->references('id')->on('user_roles')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('position_id')
				->references('id')->on('user_positions')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
