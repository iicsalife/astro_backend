<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Procedures extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

DB::unprepared('
CREATE PROCEDURE pr_get_curator_list()
BEGIN
	select
		u.id,
		u.name as fio,
		u.email,
		u.position_id,
		up.name as position_name,
		u.avatar
	from users u
	left join user_positions up on u.position_id = up.id
	where u.role_id = 2;
END');

DB::unprepared('
CREATE PROCEDURE pr_get_quiz_params(
	IN p_quiz_id INT)
BEGIN
	select
	q.id,
	q.category_id,
	c.name as category_name,
	q.title,
	q.description,
	q.curator_id,
	u.name as curator_name,	
	u.email as curator_email,
	u.position_id,
	up.name as position_name,
	q.discount
	from quiz q
	left join users u on q.curator_id = u.id
	left join categories c on q.category_id = c.id
	left join user_positions up on u.position_id = up.id
	where q.id = p_quiz_id;
END');

DB::unprepared('
CREATE PROCEDURE pr_get_tags_by_category(
	IN p_category_id INT)
BEGIN
	select
		t.id,
		t.name,
		t.category_id
	from tags t
	where t.category_id = p_category_id;
END
');

DB::unprepared('
CREATE PROCEDURE pr_get_topic_list(
	IN p_category_id INT,
	IN p_tag_list VARCHAR(255),
	IN p_page_num INT
)
BEGIN
	select *
	from communications c
	where c.category_id = p_category_id;
END
');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_curator_list');
		DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_quiz_params');
		DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_tags_by_category');
		DB::unprepared('DROP PROCEDURE IF EXISTS pr_get_topic_list');
	}
}
