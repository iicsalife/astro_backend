<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quiz')->insert([
            [
            	'category_id' => 1,
                'title' => 'Тест на уровень',
                'description' => 'Добрейшего времени суток, милый гость :)<br>Спасибо, что вы здесь. Уверены, вам понравится то, что вам предстоит. Что это?<br>Это тест. Он сначала узнаёт о вашей компании, а потом мы предлагаем решение ваших задач по маркетингу.',
                'curator_id' => 1,
                'discount' 	=> 500,
            ]
        ]);
    }
}
