<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('articles')->insert([
            [
                'id' => '1',
                'user_id' => '1',
                'category_id' => '1',
                'slug' => 'slug1',
                'title' => 'Привет из IT',
                'content' => 'Это статья о IT мире',
                'photo' => 'images/articles/...',
            ],
            [
                'id' => '2',
                'user_id' => '2',
                'category_id' => '2',
                'slug' => 'slug2',
                'title' => 'Привет из IT',
                'content' => 'Это статья о IT мире',
                'photo' => 'images/articles/...',
            ],
            [
                'id' => '3',
                'user_id' => '1',
                'category_id' => '3',
                'slug' => 'slug3',
                'title' => 'Привет из IT',
                'content' => 'Это статья о IT мире',
                'photo' => 'images/articles/...',
            ],
            [
                'id' => '4',
                'user_id' => '2',
                'category_id' => '2',
                'slug' => 'slug4',
                'title' => 'Привет из IT',
                'content' => 'Это статья о IT мире',
                'photo' => 'images/articles/...',
            ]
        ]);
    }
}
