<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(UserRoleSeeder::class);
         // $this->call(UserPositionSeeder::class);
         // $this->call(UsersSeeder::class);
         // $this->call(CategoriesSeeder::class);
         // $this->call(TagsSeeder::class);
         // $this->call(TagsSeeder::class);
         $this->call(ArticlesSeeder::class);
         $this->call(ArticleСommentsSeeder::class);
         //$this->call(ArticleFollowSeeder::class);
         $this->call(ArticleTagsSeeder::class);
         $this->call(ArtilceLikesSeeder::class);

         // $this->call(TopicsSeeder::class);
         // $this->call(TopicCommentsSeeder::class);
         // $this->call(QuizSeeder::class);
         // $this->call(QuizQuestionsSeeder::class);
         // $this->call(QuizAnswersSeeder::class);
         // $this->call(QuizAnswersUserSeeder::class);
    }
}
