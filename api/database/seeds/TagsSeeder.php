<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  		DB::table('Tags')->insert([
            [
                'name' => 'Дома',
                'category_id' => 1,
            ],
            [
                'name' => 'Планеты',
                'category_id' => 1,
            ],
            [
                'name' => 'Солнце',
                'category_id' => 1,
            ],
            [
                'name' => 'Земля',
                'category_id' => 1,
            ],

			// =====================
            [
                'name' => 'Желание',
                'category_id' =>2,
            ],
            [
                'name' => 'Методика',
                'category_id' =>2,
            ],
            [
                'name' => 'Жизнь',
                'category_id' =>2,
            ],
            // =====================
            [
                'name' => 'Аура',
                'category_id' => 3,
            ],
            [
                'name' => 'Цифра',
                'category_id' => 3,
            ],
            [
                'name' => 'отклик',
                'category_id' => 3,
            ],

            // =====================
            [
                'name' => 'Ладонь',
                'category_id' => 4,
            ],
            [
                'name' => 'Линии',
                'category_id' => 4,
            ],
            [
                'name' => 'Пальцы',
                'category_id' => 4,
            ],

        ]);
    }
}
