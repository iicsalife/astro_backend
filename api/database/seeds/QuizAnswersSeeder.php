<?php

use Illuminate\Database\Seeder;

class QuizAnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (1, 1, '0', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (6, 2, '121  нет', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (7, 3, 'Седьмой', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (8, 3, 'Двенадцатый', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (9, 3, 'Девятый', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (10, 3, 'Первый', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (11, 4, 'В том доме, где есть вода', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (12, 4, 'В доме, отвечающем за интуицию', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (13, 4, 'В любом доме, так как знаки зодиака перемещаются', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (14, 4, 'В доме, противоположном доме личности', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (15, 5, 'Лев', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (16, 5, 'Скорпион', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (17, 5, 'Стрелец', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (18, 5, 'Рыбы', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (19, 5, 'Дева', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (20, 6, 'Дхармы', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (21, 6, 'Кендры', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (22, 6, 'Упачаий', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (23, 7, 'Солнце, символизирующее знания', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (24, 7, 'Луна, символизирующая эмоции', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (25, 7, 'Сатурн, указывающий на сердечные страдания', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (26, 7, 'Венера, указывающая на супруга/супругу', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (27, 7, 'Венера, указывающая на средства передвижения', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (28, 7, 'Меркурий, символизирующий внутреннюю интуицию', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (29, 7, 'Марс, указывающий на недвижимость', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (30, 8, 'Шравана, Магха , Мула', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (31, 8, 'Ардра, Рохини, Шатабхиша', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (32, 8, 'Рохини, Хаста, Шравани', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (33, 8, 'Мригашира, Хаста, Шравани', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (34, 9, 'Учиться выносить трудности, не причинять страданий другим, развивать оптимизм', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (35, 9, 'Учиться состраданию, учиться не обижаться', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (36, 9, 'Развивать уважение к учителю, партнеру, взглядам других', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (37, 9, 'Заниматься физическим трудом', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (38, 10, 'Марс', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (39, 10, 'Меркурий', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (40, 10, 'Юпитер', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (41, 10, 'Венера', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (42, 11, 'Совмещение Юпитера и Меркурия', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (43, 11, 'Соотношение Луны и Венеры в каждой карте', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (44, 11, 'Соотношение Солнца и Луны', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (45, 11, 'Соотношение Лун в двух картах', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (46, 12, 'Нахождение Меркурия в 7 доме от Арудха Лагны (АЛ)', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (47, 12, 'Экзальтированный Меркурий, Юпитер или Венера во 2м доме от АЛ', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (48, 12, 'Марс в знаках Меркурия в 11 доме от АЛ', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (49, 12, 'Юпитер в Козероге в 7 доме от АЛ', 0, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (50, 13, 'Юпитер в Козероге в 7 доме от АЛ', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (51, 14, 'Индивидуальная (Заочно)', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (52, 14, 'Групповая (Очно)', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (53, 1, '1', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (54, 1, '2', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (55, 1, '3', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (56, 1, '4', 1, '')");
		DB::select("INSERT INTO quiz_answers (id, question_id, answer, is_correct, answer_type) VALUES (57, 1, '5', 1, '')");

    }
}
