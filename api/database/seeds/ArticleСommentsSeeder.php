<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: andreu
 * Date: 05.02.19
 * Time: 11:17
 */
class ArticleСommentsSeeder extends Seeder
{
    public function run()
    {
        DB::table('article_comments')->insert([
            [
                'article_id' => '1',
                'comment_author' => '2',
                'body' => 'Хорошая статья!',
            ],
            [
                'article_id' => '2',
                'comment_author' => '1',
                'body' => 'Хорошая статья!',
            ],
            [
                'article_id' => '1',
                'comment_author' => '3',
                'body' => 'Хорошая статья!',
            ],
            [
                'article_id' => '2',
                'comment_author' => '2',
                'body' => 'Хорошая статья!',
            ]
        ]);
    }
}