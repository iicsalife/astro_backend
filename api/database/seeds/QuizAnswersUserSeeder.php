<?php

use Illuminate\Database\Seeder;

class QuizAnswersUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (2, 1, 3, '7')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (2, 1, 2, 'tyuytu')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (2, 1, 1, '56')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (2, 1, 3, '10')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 1, '55')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 2, 'werwer')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 3, '8')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 3, '9')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 3, '7')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 4, '14')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 4, '12')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 4, '11')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 5, '17')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 5, '19')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 6, '22')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 6, '20')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 7, '27')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 7, '26')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 8, '33')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 8, '31')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 9, '37')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 10, '39')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 11, '45')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 12, '48')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 13, 'werwerwe')");
		DB::select("INSERT INTO quiz_user_answers (user_id, quiz_id, question_id, user_answer) VALUES (3, 1, 14, '51')");
    }
}
