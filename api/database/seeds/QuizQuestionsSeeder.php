<?php

use Illuminate\Database\Seeder;

class QuizQuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (1, 1, 'Сколько лет Вы изучаете Астрологию', 1, 'rating')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (2, 1, 'Какие курсы уже проходили', 2, 'comment')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (3, 1, 'В каком доме находится Асцендент?', 3, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (4, 1, 'В каком доме находится созвездие Рыб?', 4, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (5, 1, 'Представители какого знака зодиака наиболее эмоциональны, сентиментальны и наделены интуицией?', 5, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (6, 1, 'В каких домах снижается негативное действие планеты во второй половине жизни?', 6, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (7, 1, 'Какие караки четвертого дома?', 7, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (8, 1, 'Какими тремя Накшатрами владеет Луна', 8, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (9, 1, 'Какие задачи ставит перед человеком АтмаКарака Сатурн?', 9, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (10, 1, 'Какой подпериод будет вторым в Махадаше Юпитера?', 10, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (11, 1, 'За эмоциональную и психологическую совместимость партнеров отвечает', 11, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (12, 1, 'Какая комбинация может сделать натива богатым', 12, 'checkbox')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (13, 1, 'Что измениться в Вашей жизни после изучения Астрологии?', 13, 'comment')");
		DB::select("INSERT INTO quiz_questions (id, quiz_id, question, orders, question_type) VALUES (14, 1, 'Какая форма изучения, в настоящий момент, Вам удобнее?', 14, 'radiogroup')");
    }
}
