<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorFollowSeeder extends Seeder
{
    public function run()
    {
        DB::table('author_follow')->insert([
            [
                'article_id' => '1',
                'user_id' => '2',
            ],
            [
                'article_id' => '1',
                'user_id' => '3',
            ],
            [
                'article_id' => '2',
                'user_id' => '1',
            ],
            [
                'article_id' => '3',
                'user_id' => '1',
            ]
        ]);
    }
}