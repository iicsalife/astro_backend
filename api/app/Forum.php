<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Cviebrock\EloquentSluggable\Sluggable;
use \Cviebrock\EloquentSluggable\Services\SlugService;

use App\Tools;

class Forum extends Model {

	use Sluggable;

	protected $table = 'topics';
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'category_id' => 'integer',
		'title' => 'string',
		'slug'	=> 'string',
		'body' => 'string',
		'follow' => 'integer',
		'views' => 'integer',
		'answer' => 'integer',
	];

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public static function getSlug( $title ) {    
		return SlugService::createSlug(Forum::class, 'slug', $title);
	}

	public static function getTopicList( $data ) {
		return DB::select( 'call pr_get_topic_list( :category_id, :tag_list, :page_num )', [
			':category_id'  => $data['category_id'],
			':tag_list'     => $data['tag_list'],
			':page_num'     => $data['page_num'],
		]);
	}

	public static function getTopic( $topicId ) {
		$topic = DB::select( 'call pr_get_topic( :topic_id )', [
			':topic_id'  => $topicId,
		]);

		$comments = self::getTopicMoreComments([
			"topic_id" => $topicId, 
			"page_num" => 0
		]);

		return array_merge( (array)$topic[0], (array)$comments );
	}

	public static function getTopicMoreComments( $data ) {
		$comments = DB::select( 'call pr_get_topic_more_comments( :topic_id, :page_num )', [
			':topic_id'  => $data['topic_id'],
			':page_num'  => $data['page_num'],
		]);

		$is_next = DB::select( 'select fu_is_comments_next( :topic_id, :page_num, :page_type ) as result', [
			':topic_id'  => $data['topic_id'],
			':page_num'  => $data['page_num'],
			':page_type' => 'FORUM',
		]);

		return ['is_next' => $is_next[0]->result, 'comments' => $comments];
	}

	public static function setTopicView( $data ) {
		return DB::select( 'call pr_set_topic_view( :topic_id  )', [
			':topic_id'  	=> $data['topic_id'],
		]);
	}


	public static function addTopic( $userId, $data ) {

		$model = DB::select( 'call pr_add_topic( :user_id, :category_id, :slug, :title, :body )', [
			':user_id'  	=> $userId,
			':category_id'  => $data['category_id'],
			':slug' 		=> self::getSlug( $data['title'] ),
			':title' 		=> $data['title'],
			':body' 		=> $data['body'],
		])[0];

		$tags = explode( ',', $data['tag_list'] );

		for ($i=0; $i < count($tags); $i++) { 
			DB::select( 'call pr_set_topic_tag( :topic_id, :tag_id )', [
				':topic_id' => $model->topic_id,
				':tag_id' 	=> $tags[$i],
			]);
		}

		//$data['seo_description'] = $data['seo_description'] ?? substr($data['body'], 0, 250) . '...';
		Tools::setSeoParams( $model->topic_id, 'forum', $data );
		return $model;
	}

	public static function addTopicComment( $userId, $data ) {
		return DB::select( 'call pr_add_topic_comment( :topic_id, :user_id, :comment_body, :parent_id )', [
			':topic_id' 	=> $data['topic_id'],
			':user_id'  	=> $userId,
			':comment_body'	=> $data['comment_body'],
			':parent_id'	=> $data['parent_id'],
		]);
	}

	public static function setTopicLike( $userId, $data ) {
		return DB::select( 'call pr_set_topic_like( :topic_id, :user_id, :is_like )', [
			':topic_id' => $data['topic_id'],
			':user_id'  => $userId,
			':is_like'	=> $data['is_like'],
		]);
	}

	public static function setTopicCommentLike( $userId, $data ) {
		return DB::select( 'call pr_set_topic_comment_like( :topic_comment_id, :user_id, :is_like )', [
			':topic_comment_id' => $data['topic_comment_id'],
			':user_id'  		=> $userId,
			':is_like'			=> $data['is_like'],
		]);
	}

	public static function setTopicFollow( $currUser, $data ) {
		return DB::select( 'call pr_set_topic_follow( :topic_id, :user_id, :is_subs )', [
			':topic_id' => $data['topic_id'],
			':user_id'  => $currUser,
			':is_subs'	=> $data['is_subs'],
		]);
	}


// =================================================================
// =================================================================
// =================================================================


	static public function toggleFollow($object)
	{
		if (!$object->viewFollow) {
			Forum::where('id', '=', $object->communication_id)->update(['status_follow' => true, 'follow' => ($object->follow + 1)]);
		} else {
			Forum::where('id', '=', $object->communication_id)->update(['status_follow' => false, 'follow' => ($object->follow - 1)]);
		}
	}

	static public function toggleView($object)
	{
		if (!$object->viewStatus) {
			Forum::where('id', '=', $object->communication_id)->update(['status_views' => true, 'views' => ($object->views + 1)]);
		} else {
			Forum::where('id', '=', $object->communication_id)->update(['status_views' => false, 'views' => ($object->views - 1)]);
		}
	}


	// static public function getFilteringCommunications($arr)
	// {
	//     $communications = Forum::where(function ($query) use ($arr)
	//     {
	//         foreach($arr as $category_id) {
	//             $query->orWhere('category_id', '=', $category_id);
	//         }
	//     })->get();

	//     return $communications;
	// }

	static public function getGlobalFilteringCommunications($category, $globalCategory)
	{
		switch ($globalCategory) {
			case 'top':
				$communications = Forum::orderBy('views', 'DESC')->orderBy('answer', 'DESC')->get();
				return $communications;
			case 'follow':
				return 2;
			case 'new':
				$communications = Forum::orderBy('created_at', 'DESC')->get();
				return $communications;
			case 'waiting':
				$communications = Forum::where('answer', '=', 0)->get();
				return $communications;
		}
	}

	static public function getFilteringAndGlobalFiltringCommunications($category, $globalCategory)
	{
		$queryCommunication = Forum::where(function ($query) use ($category)
		{
			foreach($category as $category_id) {
				$query->orWhere('category_id', '=', $category_id);
			}
		});

		switch ($globalCategory) {
			case 'top':
				return $queryCommunication->orderBy('views', 'DESC')->orderBy('answer', 'DESC')->get();
			case 'follow':
				return 2;
			case 'new':
				return $queryCommunication->orderBy('created_at', 'DESC')->get();
			case 'waiting':
				return $queryCommunication->where('answer', '=', 0)->get();
		}
	}

}