<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ArticleComment extends Model
{
    protected $table = "article_comments";

    public static function addComment($data)
    {
        return DB::select('call pr_add_article_comment(:article_id, :comment_author, :comment_body, :parent_id)', [
            ':article_id'         => $data['article_id'],
            ':comment_author' => $data['comment_author'],
            ':comment_body'     => $data['comment_body'],
            ':parent_id'           => $data['parent_id'],
        ]);
    }

    public static function getComments($article_id)
    {
        return DB::select('call pr_get_article_comment('.$article_id.')');
    }
}
