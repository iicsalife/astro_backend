<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Topics extends Model {
	
	public static function getTopicList( $data ) {
		return DB::select( 'call pr_get_topic_list( :category_id, :tag_list, :page_num )', [
			':category_id' 	=> $data['category_id'],
			':tag_list' 	=> $data['tag_list'],
			':page_num' 	=> $data['page_num'],
		]);
	}
}
