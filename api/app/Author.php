<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Author extends Model
{
    public static function getAuthor( $author_id, $user_id)
    {
        return DB::select( 'call pr_get_author('.$author_id.', '.$user_id.')');
    }

    public static function setFollower( $data )
    {
        return DB::select( 'call pr_set_follower( :author_id , :user_id)', [
            ':author_id'  => $data['author_id'],
            ':user_id'  => $data['user_id'],
        ]);
    }
}
