<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'communication_id' => 'integer',
        'comment' => 'string',
        'parent_id' => 'integer'
    ];

    static public function getAllComments($id) {
        return Comments::where('communication_id', '=', $id)->get();
    }

    static public function addNewComment($object)     {
        $newRow = new Comments;
        $newRow->communication_id = $object->communication_id;
        $newRow->comment = $object->newComment;

        $newRow->save();
    }

    public function communication() {
        return $this->belongsTo(Communication::class, 'communication_id', 'id');
    }
}
