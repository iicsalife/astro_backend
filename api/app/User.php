<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable {
	use Notifiable, HasApiTokens, HasRoles;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public static function getAuthor( $author_id, $user_id) {
		return DB::select( 'call pr_get_author(:author_id , :user_id)', [
			':author_id'=> $author_id,
			':user_id' 	=> $user_id,
		]);
	}

	public static function setFollower( $data )
	{
		return DB::select( 'call pr_set_follower( :author_id , :user_id)', [
			':author_id'  => $data['author_id'],
			':user_id'  => $data['user_id'],
		]);
	}
	public static function delFollower($data)
	{
		return DB::select( 'call pr_del_follower( :author_id , :user_id)', [
			':author_id'  => $data['author_id'],
			':user_id'  => $data['user_id'],
		]);
	}
	// public static function getCuratorList() {
	//     return DB::select( 'call pr_get_curator_list()');
	// }


	// public static function getCuratorList() {
	//     return DB::select( 'call pr_get_curator_list( :category_id, :tag_list, :page_num )', [
	//         ':category_id'  => $data['category_id'],
	//         ':tag_list'     => $data['tag_list'],
	//         ':page_num'     => $data['page_num'],
	//     ]);
	//     return DB::select( 'call pr_get_curator_list(?)', [ $id ] );
	// }


}
