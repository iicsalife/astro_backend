<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Tags;
use App\Tools;

class ToolsController extends Controller {

	const SEO_NO_FOUND = 'SEO data not found';

	public function getCategoriesWithTags() {
		$catList = Category::getCategoryList();
		
		$tag_all = [];
		$tag_all[0]['id'] = 0;
		$tag_all[0]['name'] = 'Все';

		for ($i=0; $i < count($catList); $i++) { 
			$catList[$i]->tags = array_merge($tag_all, Tags::getTagsByCategory( $catList[$i]->id ) );
		}
		return $catList;
	}

	public function getCuratorList() {
		return Tools::getCuratorList();
	}

	public function getSeoParams( Request $request ) {
		$params = Tools::getSeoParams( $request );

		if ( !count($params) ) return $this->getError(404, 101, self::SEO_NO_FOUND);
		
		$params = $params[0];

		$result['title'] = $params->title;
		$result['meta'] = [
			["charset" => "utf-8"],
			["name" => "wiewport", "content" => "width-device-width, initial-scale=1"],
			["name" => "fb:app_id", "content" => "..."],
			["hid" => "og:url", "name" => "og:url", "content" => "astro.icsa-life.ru"],
			["hid" => "og:type", "name" => "og:type", "content" => "website"],
			["hid" => "og:image", "name" => "og:image", "content" => env('SHARE_IMAGE', false)],
			["hid" => "og:site_name", "name" => "og:site_name", "content" => "Astrology"],
			["hid" => "keywords", "name" => "keywords", "content" => $params->keywords],
			["hid" => 'description', "name"	=> 'description', "content" => $params->description],
		];
	
		return $this->getSuccess( $result );
	}

}
