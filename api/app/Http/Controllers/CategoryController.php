<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller {
    
    public function getCategoryList( Request $request ) {
        return Category::getCategoryList();
    }
}
