<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;

class QuizController extends Controller {

	// ==================================================================================================================
	// CLIENT SIDE
	// ==================================================================================================================

	public static function addQuiz( Request $request ) {
		return Quiz::addQuiz( $request );
	}

	public static function updateQuiz( Request $request ) {
		return Quiz::updateQuiz( $request );
	}

	public static function getList( Request $request ) {
		$result = Quiz::getList( 
			($request->is_draft) ? 1 : 0,
			($request->is_contact) ? 1 : 0
		);

		for ($i=0; $i < count($result); $i++) {
			$result[$i]->tag_list = ( $result[$i]->tag_list ) ? explode(',', $result[$i]->tag_list) : [];
		}

		return $result;
	}

	public static function getQuizData( Request $request ) {
		$result = Quiz::getQuizData( $request->quiz_id );
		$result[0]->tag_list = ( $result[0]->tag_list ) ? explode(',', $result[0]->tag_list) : [];
		$result[0]->curator_photo =  env('CURATOR_PHOTO_PATCH', false) . $result[0]->curator_photo;
		return $result;
	}

	public static function setQuizUserAnswers( Request $request ) {
		return Quiz::setQuizUserAnswers( $request );
	}



	// ==================================================================================================================
	// ADMIN SIDE
	// ==================================================================================================================

	public static function setActive( Request $request ) {
		return Quiz::setActive( $request );
	}

	public static function getQuizUserAnswers( Request $request ) {
		$quizId  = ($request->quiz_id) ? $request->quiz_id : 0;
		return Quiz::getQuizUserAnswers( $request->user_id, $quizId );
	}


	// public static function getQuestions( Request $request ) {
	// 	$params = Quiz::getQuestions( $request->quiz_id );
	// 	for ($i=0; $i < count($params) ; $i++) { 
	// 		$params[$i]->choices = json_encode($params[$i]->choices);
	// 		$params[$i]->correct_answers = explode(',', $params[$i]->correct_answers);
	// 	}
	// 	return $params;
	// }
}
