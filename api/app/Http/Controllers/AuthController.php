<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthController extends Controller {

	/**
	 * Create user
	 *
	 * @param  [string] name
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [string] password_confirmation
	 * @return [string] message
	 */
	public function signup( Request $request ) {

		$request->validate([
			'name' => 'required|string',
			'email' => 'required|string|email|unique:users',
			'password' => 'required|string|confirmed'
		]);
		$user = new User([
			'name' => $request->name,
			'email' => $request->email,
			'password' => bcrypt($request->password)
		]);
		$user->save();

		return $this->getSuccess( 'Successfully created user' );

		// return response()->json([
		// 	'message' => 'Successfully created user!'
		// ], 201);
	}

	/**
	 * Login user and create token
	 *
	 * @param  [string] email
	 * @param  [string] password
	 * @param  [boolean] remember_me
	 * @return [string] access_token
	 * @return [string] token_type
	 * @return [string] expires_at
	 */
	public function login( Request $request ) {
		
		$request->validate([
			'email' => 'required|string|email',
			'password' => 'required|string',
			'remember_me' => 'boolean'
		]);
		
		$credentials = request(['email', 'password']);
		if(!Auth::attempt($credentials))
			return response()->json([
				'message' => 'Пользователь не найден, проверьте email, password.'
			], 401);
		$user = $request->user();
		$tokenResult = $user->createToken('Personal Access Token');
		$token = $tokenResult->token;
		if ($request->remember_me)
			$token->expires_at = Carbon::now()->addWeeks(1);
		$token->save();
		return $this->getSuccess([
			'access_token' => $tokenResult->accessToken,
			'token_type' => 'Bearer',
			'expires_at' => Carbon::parse(
				$tokenResult->token->expires_at
			)->toDateTimeString()
		]);
	}

	/**
	 * Logout user (Revoke the token)
	 *
	 * @return [string] message
	*/
	public function logout( Request $request ) {
		$request->user()->token()->revoke();
		return $this->getSuccess( 'Successfully logged out' );
	}

	/**
	 * Get the authenticated User
	 *
	 * @return [json] user object
	 */
	public function getUser( Request $request ) {
//        Role::create(['name' => "super_admin"]);
//        Permission::create(['name' => "create role"]);
//        $role = Role::findById(3);
//        $permission = Permission::findById(1);
//        $role->givePermissionTo($permission);
//        $role->givePermissionTo(Permission::all());
		//$user = User::find(13);
		//$user->assignRole('enrollee');
		$user = $request->user();
		$roles = $user->getRoleNames();
		return $this->getSuccess( $request->user() );
	}

    public static function createRole( Request $request ) {

        $request->validate([
            'role_name' => 'required|string',
        ]);

		$role = Role::create(['name' => $request->role_name]);
    	return $this->getSuccess( 'Role was created' );
    }

	public function createPriv( Request $request ) {
		Permission::create(['name' => $request->priv_name]);
		return $this->getSuccess( 'Priv was created' );
	}

	public function addPrivToRole( Request $request ) {
		$role = Role::findById( $request->role_id );
		$role->givePermissionTo( $request->priv_id );
		return $this->getSuccess( 'Priv assigned to Role' );
	}

	public function addRoleToUser( Request $request ) {
		$user = User::find( $request->user_id );
		$user->assignRole( $request->role_name );
		return $this->getSuccess( 'Role assigned to User' );
	}

	public function getRoleList( Request $request ) {
		return $this->getSuccess( Role::all() );
	}

	public function getPrivList( Request $request ) {
		return $this->getSuccess( Permission::all() );
	}

	public function unbindRoleFromUser( Request $request ) {
		$user = User::find( $request->user_id );
		$user->removeRole( $request->role_name );
		return $this->getSuccess();
	}

	public function unbindPrivFromRole( Request $request ) {
		// $role = User::find( $request->role_id );
		// $role->revokePermissionTo( $request->priv_name );
		$permission = Permission::findById( $request->priv_id );
		$permission->removeRole( $request->role_name );
		return $this->getSuccess();
	}

	
}