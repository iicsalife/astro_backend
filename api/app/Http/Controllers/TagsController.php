<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tags;

class TagsController extends Controller {
	
	public static function getTagsByCategory(  Request $request ) {
		return Tags::getTagsByCategory( $request->category_id ) ;
	}

	public static function getTagList() {
		return Tags::getTagList();
	}
}
