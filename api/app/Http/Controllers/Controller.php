<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	const ERROR_EXECUTION_ERROR = 'Error happend while processing your request.';

	/**
		* @param string  	$content	Response body
	*/
	protected function getSuccess( $content = '' ) {
		return [
			'status'    => true,
			'content'   => $content
		];
	}


	// /**
	// 	* @param string  	$error		Your message (beter if are const)
	// */
	// protected function getError( $error = self::ERROR_EXECUTION_ERROR ) {
	// 	return [
	// 		'status'    => false,
	// 		'content'   => $error
	// 	];
	// }

	/**
	 * @param int  		$statusCode		HTML status code
	 * @param int  		$errorCode		Your local code error (validation or etc.)
	 * @param string  	$message		Your message (beter if are const)
	*/
	protected function getError( $statusCode, $errorCode, $message = '' ) {

		return response()->json([
			'status'    => false,
			'content'   => [
				'code'		=> $errorCode,
				'message'   => $message
			]
		], $statusCode);

	}
}
