<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Forum;

class ForumController extends Controller {

	public function getTopicList( Request $request ) {
		return Forum::getTopicList( $request );
	}

	public function getTopic(Request $request) {
		return $this->getSuccess(
			Forum::getTopic( $request->topic_id )
		);
	}

	public function getTopicMoreComments(Request $request) {
		return $this->getSuccess(
			Forum::getTopicMoreComments( $request )
		);
	}

	public function addTopic( Request $request ) {
		return $this->getSuccess(
			// Forum::addTopic( Auth::user()->id, $request )
			Forum::addTopic( 10, $request )
		);
	}

	public function addTopicComment( Request $request ) {
		return $this->getSuccess(
			// Forum::addTopic( Auth::user()->id, $request )
			Forum::addTopicComment( 10, $request )
		);
	}

	public function setTopicView( Request $request ) {
		return $this->getSuccess(
			Forum::setTopicView( $request )
		);
	}

	public function setTopicLike( Request $request ) {
		return $this->getSuccess(
			//Forum::setTopicLike( Auth::user()->id, $request )
			Forum::setTopicLike( 10, $request )
		);
	}

	public function setTopicCommentLike( Request $request ) {
		return $this->getSuccess(
			//Forum::setTopicCommentLike( Auth::user()->id, $request )
			Forum::setTopicCommentLike( 10, $request )
		);
	}

	public function setTopicFollow( Request $request ) {
		return $this->getSuccess(
			//Forum::setTopicFollow( Auth::user()->id, $request )
			Forum::setTopicFollow( 10, $request )
		);
	}


}