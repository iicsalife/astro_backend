<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Forum;
use App\Comments;

class CommentController extends Controller {


	public function toggleFollow(Request $request) {
		Forum::toggleFollow($request);
	}

	public function toggleView(Request $request) {
		Forum::toggleView($request);
	}

	public function getTopicComments(Request $request) {
		return Forum::getTopic($request->comments_id);
	//$comments = Comments::getAllComments($request->comments_id);
	//return response()->json(['comments' => $comments, 'communications' => $communications]);
	}

	public function addNewComment(Request $request) {
		Comments::addNewComment($request);
	}
}