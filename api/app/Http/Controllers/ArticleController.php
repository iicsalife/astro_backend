<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\ArticleComment;
use App\User;
use Illuminate\Support\Facades\Hash;

class ArticleController extends Controller {

    public function getArticleList(Request $request) {
        return $this->getSuccess(
            Article::getArticleList($request)
        );
    }

    public function getArticle(Request $request)
    {
//        $result =  Article::getArticle($request);
//        $result[0]->author = User::getAuthor($result[0]->author ,$request->user_id);
//        $result[0]->article_comment = ArticleComment::getComments($result[0]->id);
        $result = Article::getArticle($request);
        $result = $result;
        $result[0]->author = User::getAuthor($result[0]->author, $request->user_id);
        $result[0]->article_comment = Article::getComments($result[0]->id);
        return $result;
    }

    public function addComment(Request $request)
    {
        return $this->getSuccess(
            Article::addComment($request)
        );
    }

    public function setLike(Request $request) {
        if($request->is_like == 1){
            return $this->getSuccess(
                Article::setLike($request)
            );
        } else{
            return $this->getSuccess(
                Article::disLike($request)
            );
        }
    }
    
    public function setFollower(Request $request)
    {
        if($request->set_follow == 1){
            return $this->getSuccess(
                User::setFollower($request)
            );
        } else{
            return $this->getSuccess(
                User::delFollower($request)
            );
        }
    }

    public function addArticle(Request $request)
    {
        $types = ['image/jpg', 'image/jpeg', 'image/png'];
        if(in_array($_FILES['photo']['type'], $types)) {
            $fileName = md5(microtime() . rand(0, 9999));
            $type = explode('.', $_FILES['photo']['name']);
            $fileName .= '.' . $type[1];
            if (is_uploaded_file($_FILES["photo"]["tmp_name"]) && $_FILES['photo']['size'] < 3 * 1024 * 1024) {
                move_uploaded_file($_FILES["photo"]["tmp_name"], env('ARTICLE_IMAGE_PATH', false) .'/'. $fileName);
            } else {
                echo("Ошибка загрузки файла");
            }
            $photo = env('ARTICLE_IMAGE_PATH', false) .'/'. $fileName;
            return Article::addArticle($request, $photo);
        }
    }

    public function updateArticle(Request $request)
    {
        $types = ['image/jpg', 'image/jpeg', 'image/png'];
        if(in_array($_FILES['photo']['type'], $types)){
            $fileName = md5(microtime() . rand(0, 9999));
            $type = explode('.', $_FILES['photo']['name']);
            $fileName .= '.'.$type[1];
            if(is_uploaded_file($_FILES["photo"]["tmp_name"]) && $_FILES['photo']['size'] < 3 * 1024 * 1024)
            {
                $article = Article::find($request->id);
                unlink($article->photo);
                move_uploaded_file($_FILES["photo"]["tmp_name"], env('ARTICLE_IMAGE_PATH', false) .'/'.$fileName);
            } else {
                echo("Ошибка загрузки файла");
            }
            $photo = env('ARTICLE_IMAGE_PATH', false) .'/'. $fileName;
            Article::deleteTags($request->id);
            return Article::updateArticle($request, $photo);
        }
    }
    
    public function setArticleStatus(Request $request)
    {
        return $this->getSuccess(
            Article::setArticleStatus($request)
        );
    }

    public function showIndexPage()
    {
        return view('index');
    }
}
