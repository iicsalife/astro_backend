<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tags extends Model{
    
    public static function getTagsByCategory( $categoryId ) {
		return DB::select( 'call pr_get_tags_by_category(?)', [ $categoryId ] );
	}

    public static function getTagList() {
		return DB::select( 'call pr_get_tag_list()');
	}
	
}
