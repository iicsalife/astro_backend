<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Article extends Model {

    public static function getArticleList( $data )  {
        return DB::select( 'call pr_get_article_list( :category_id, :tag_list, :page_num )', [
            ':category_id'  => $data['category_id'],
            ':tag_list'     => $data['tag_list'],
            ':page_num'     => $data['page_num'],
        ]);
    }

    public static function getArticle( $data ) {
        return DB::select( 'call pr_get_article( :article_id , :user_id)', [
            ':article_id'  => $data['article_id'],
            ':user_id'  => $data['user_id'],
        ]);
    }

    public static function setLike( $data )
    {
        return DB::select( 'call pr_set_article_like( :article_id , :user_id)', [
            ':article_id'  => $data['article_id'],
            ':user_id'  => $data['user_id'],
        ]);
    }

    public static function disLike( $data )
    {
        return DB::select( 'call pr_dis_like_article( :article_id , :user_id)', [
            ':article_id'  => $data['article_id'],
            ':user_id'  => $data['user_id'],
        ]);
    }

    public static function addArticle( $data, $photo)
    {
        $result = DB::select( 'call pr_add_article(:author_id,:category_id,:slug,:title,:content,:photo)', [
            ':category_id'  => $data['category_id'],
            ':author_id'  => $data['author_id'],
            ':slug' => $data['slug'],
            ':title'  => $data['title'],
            ':content'  => $data['content'],
            ':photo'  => $photo,
        ]);
        $tags = explode(',', $data['tag_list']);
        for($i=0;$i<count($tags);$i++){
            DB::select('call pr_add_tag_article(:article_id,:tag_id)',[
                ':article_id' => $result[0]->article_id,
                ':tag_id' => $tags[$i],
            ]);
        }
    }
    
    public static function updateArticle( $data, $photo )
    {
        $tags = explode(',', $data['tag_list']);
        for($i=0;$i<count($tags);$i++){
            DB::select('call pr_add_tag_article(:article_id,:tag_id)',[
                ':article_id' => $data['id'],
                ':tag_id' => $tags[$i],
            ]);
        }
        return DB::select( 'call pr_update_article(:article_id,:category_id,:author_id,:title,:content,:photo)', [
            ':article_id'  => $data['id'],
            ':category_id'  => $data['category_id'],
            ':author_id'  => $data['author_id'],
            ':title'  => $data['title'],
            ':content'  => $data['content'],
            ':photo'  => $photo,
        ]);
    }

    public static function setArticleStatus( $data )
    {
        return DB::select( 'call pr_set_article_status( :article_id , :is_active)', [
            ':article_id'  => $data['article_id'],
            ':is_active'  => $data['is_active'],
        ]);
    }

    public static function deleteTags($article_id)
    {
        DB::table('article_tags')->where('article_id', $article_id)->delete();
    }

    public static function addComment($data)
    {
        return DB::select('call pr_add_article_comment(:article_id, :comment_author, :body, :parent_id)', [
            ':article_id'         => $data['article_id'],
            ':comment_author' => $data['comment_author'],
            ':body'     => $data['comment_body'],
            ':parent_id'           => $data['parent_id'],
        ]);
    }

    public static function getComments($article_id)
    {
        return DB::select('call pr_get_article_comment('.$article_id.')');
    }
}
