<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tools extends Model {
	
	public static function getCuratorList() {
		return DB::select( 'call pr_get_curator_list()');
	}

	public static function getSeoParams( $data ) {
		return DB::select( 'call pr_get_seo_params( :seo_page, :seo_id )', [
			':seo_page' => $data['seo_page'],
			':seo_id'  	=> $data['seo_id'],
		]);
	}

	public static function setSeoParams( $id, $page, $data ) {
		DB::select( 'call pr_set_seo_params( :id, :page, :title, :description, :keywords )', [
			':id'			=> $id,
			':page'  		=> $page,
			':title' 		=> $data['title'],
			':description'	=> ($data['seo_description']) ? $data['seo_description'] : '',
			':keywords' 	=> ($data['seo_keywords']) ? $data['seo_keywords'] : '',
		]);
	}

}
