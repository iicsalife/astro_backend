<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Quiz extends Model {

	// ==================================================================================================================
	// CLIENT SIDE
	// ==================================================================================================================


	public static function getList( $isDraft, $isContact ) {
		return DB::select( 'call pr_get_quiz_list( :is_draft, :is_contact )', [
			':is_draft' 	=> $isDraft,
			':is_contact' 	=> $isContact,
		]);
	}

	public static function getQuizData( $quizId ) {
		return DB::select( 'call pr_get_quiz_data( :quiz_id )', [ 
			':quiz_id' => $quizId
		]);
	}

	public static function setQuizUserAnswers( $data ) {
		return DB::select( 'call pr_set_quiz_user_answers( :user_id, :quiz_id, :answers, :is_contact, :user_name, :user_email, :user_phone )', [
			':user_id' 		=> $data['user_id'],
			':quiz_id' 		=> $data['quiz_id'],
			':answers' 		=> $data['answers'],
			':is_contact' 	=> $data['is_contact'],
			':user_name' 	=> $data['is_contact'] ? $data['user_name']  : '',
			':user_email' 	=> $data['is_contact'] ? $data['user_email'] : '',
			':user_phone' 	=> $data['is_contact'] ? $data['user_phone'] : '',
		]);
	}




	// ==================================================================================================================
	// ADMIN SIDE
	// ==================================================================================================================

	protected static function upadteQuizTags( $quizId, $tagList ) {
		if ( count($tagList) ) {
			for ($i=0; $i < count($tagList); $i++) { 
				DB::select( 'call pr_add_tag_to_quiz( :quiz_id, :tag_id )', [
					':quiz_id' 	=> $quizId,
					':tag_id'	=> $tagList[$i],
				]);
			}
		}
	}

	public static function addQuiz( $data ) {
		$result = DB::select( 'call pr_add_quiz( :category_id, :title, :description, :curator_id, :curator_status, :discount, :is_contact, :quiz_data )', [
			':category_id'  	=> $data['category_id'],
			':title' 			=> $data['title'],
			':description'  	=> $data['description'],
			':curator_id'   	=> $data['curator_id'],
			':curator_status'   => $data['curator_status'],
			':discount' 		=> $data['discount'],
			':is_contact'		=> $data['is_contact'],
			':quiz_data'		=> json_encode($data['quiz_data']),
		]);

		self::upadteQuizTags( $result[0]->quiz_id, $data['tag_list'] );
		return $result;
	}

	public static function updateQuiz( $data ) {
		DB::select( 'call pr_update_quiz( :quiz_id, :category_id, :title, :description, :curator_id, :curator_status, :discount, :is_contact, :quiz_data )', [
			':quiz_id'  		=> $data['quiz_id'],
			':category_id'  	=> $data['category_id'],
			':title' 			=> $data['title'],
			':description'  	=> $data['description'],
			':curator_id'   	=> $data['curator_id'],
			':curator_status'   => $data['curator_status'],
			':discount' 		=> $data['discount'],
			':is_contact'   	=> $data['is_contact'],
			':quiz_data'		=> json_encode($data['quiz_data']),
		]);

		DB::select( 'call pr_delete_quiz_tags( :quiz_id )', [':quiz_id' => $data['quiz_id'] ]);
		self::upadteQuizTags( $data['quiz_id'], $data['tag_list'] );
		return 1;
	}
	
	public static function setActive( $data ) {
		return DB::select( 'call pr_quiz_set_active( :quiz_id, :is_active )', [
			':quiz_id'  	=> $data['quiz_id'],
			':is_active' 	=> $data['is_active'],
		]);
	}

	public static function getQuizUserAnswers( $userId, $quizId ) {
		return DB::select( 'call pr_get_quiz_user_answers( :user_id, :quiz_id )', [ 
			':user_id' => $userId,
			':quiz_id' => $quizId,
		]);
	}
}
