<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('tools')->group(function () {
	Route::post('get-categories-with-tags', 'ToolsController@getCategoriesWithTags');
	Route::post('get-curator-list', 'ToolsController@getCuratorList');
	Route::post('get-seo-params', 'ToolsController@getSeoParams');
});

Route::prefix('category')->group(function () {
	Route::post('get-category-list', 'CategoryController@getCategoryList');
});

Route::prefix('tags')->group(function () {
	Route::post('get-tags-by-category', 'TagsController@getTagsByCategory');
	Route::post('get-tag-list', 'TagsController@getTagList');
});


// =============== FORUM ===============
Route::prefix('forum')->group(function () {
	Route::post('get-topic-list', 'ForumController@getTopicList');
	Route::post('get-topic', 'ForumController@getTopic');
	Route::post('get-topic-more-comments', 'ForumController@getTopicMoreComments');

	Route::post('add-topic', 'ForumController@addTopic');
	Route::post('add-topic-comment', 'ForumController@addTopicComment');

	Route::post('set-topic-like', 'ForumController@setTopicLike');
	Route::post('set-topic-comment-like', 'ForumController@setTopicCommentLike');
	Route::post('set-topic-follow', 'ForumController@setTopicFollow');
	Route::post('set-topic-view', 'ForumController@setTopicView');

});
//=================BLOG=================
Route::prefix('blog')->group(function () {
	Route::post('get-article-list', 'ArticleController@getArticleList');
	Route::post('get-article', 'ArticleController@getArticle');
	Route::post('add-comment', 'ArticleController@addComment');
	Route::post('set-like', 'ArticleController@setLike');
	Route::post('dis-like', 'ArticleController@disLike');
	Route::post('set-follower', 'ArticleController@setFollower');
	Route::post('add-article', 'ArticleController@addArticle');
	Route::post('update-article', 'ArticleController@updateArticle');
	Route::post('set-article-status', 'ArticleController@setArticleStatus');
});

// =============== QUIZ ===============
Route::prefix('quiz')->group(function () {
	// client side
	Route::post('add', 'QuizController@addQuiz');
	Route::post('update', 'QuizController@updateQuiz');
	Route::post('get-list', 'QuizController@getList');
	Route::post('get-quiz-data', 'QuizController@getQuizData');
	Route::post('set-quiz-user-answers', 'QuizController@setQuizUserAnswers');

	// admin side
	Route::post('set-active', 'QuizController@setActive');
	Route::post('get-quiz-user-answers', 'QuizController@getQuizUserAnswers');
});
//=================AUTH===================
;

Route::group(['prefix' => 'auth'], function () {
	Route::post('login', 'AuthController@login')->name("login");
	Route::post('signup', 'AuthController@signup');
});

Route::group([
	'middleware' => 'auth:api'
], function() {

	Route::group(['prefix' => 'auth'], function () {
		Route::post('logout', 'AuthController@logout');
		Route::post('get-user', 'AuthController@getUser');
	});

	//=========================SUPER ADMIN============================
	Route::group(['prefix' => 'auth', 'middleware' => ['role:super_admin']], function () {
		Route::post('create-role', 'AuthController@createRole');
		Route::post('create-priv', 'AuthController@createPriv');
		Route::post('add-priv-to-role', 'AuthController@addPrivToRole');
		Route::post('add-role-to-user', 'AuthController@addRoleToUser');
		Route::post('get-role-list', 'AuthController@getRoleList');
		Route::post('get-priv-list', 'AuthController@getPrivList');
		Route::post('unbind-role-from-user', 'AuthController@unbindRoleFromUser');
		Route::post('unbind-priv-from-role', 'AuthController@unbindPrivFromRole');

		
	});

	//=========================ADMIN============================

	Route::group(['middleware' => ['role:admin']], function () {
	});

	//=========================Manager============================
	Route::group(['middleware' => ['role:manager']], function () {
	});

	//=========================Curator============================
	Route::group(['middleware' => ['role:curator']], function () {
	});

	//=========================Student============================
	Route::group(['middleware' => ['role:student']], function () {
	});

	//===========================USER(Абитуриент)===================================
	Route::group(['middleware' => ['role:user']], function () {
	});
});



