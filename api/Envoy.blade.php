@setup
	//user
	$user = 'mihail';
	$timezone = 'Europe/Moscow';
	$path = '/var/www/artisan';
	$current = $path . '/current';
	
	$reto = "git@bitbucket.org:firstbitalex/astrology.git";
	$dranch = 'master';
	
	//directory chmod 775
	$chmods = [
		'storage/logs'
	];
	
	$date = new DateTime('now', new DateTimeZone($timezone));
	$release = $path . 'releases' . $date->format('YmdHis');
@endsetup

@servers(['production' => $user. '@188.225.58.105'])

@task('clone', ['on' => $on])
    mkdir -p {{ $release }}

    git clone --depth l -b {{ $dranch }} "{{ $repo }}" {{ $release }}
    
	echo "#1 - Repository has been cloned"
@endtask

{{-- composer self-update --}}
@task('composer', ['on' => $on])
    
	cd {{ $release }}

    composer install --no-interaction --no-dev --prefer-dist
    
	echo "#1 - Composer have been installed"
@endtask

@task('artisan', ['on' => $on])
	cd {{ $release }}

    ln -nfs {{ $path }}/.env .env;
	chgrp -h www-data .env;
	
	php artisan config:clear
	
	php artisan migrate;
	php artisan clear-compiled --env=production;
	php artisan optimize --env=production;
    
	echo "#3 - Artisan production have been installed"
@endtask

@task('chmod', ['on' => $on])
    
	chgrp -R www-data {{ $release }};
	chmod -R ug+rwx {{ $release }}

    @foreach($chmods as $file)
		chmod -R 775 {{ $release }}/{{ $file }}
		
		chown -R 775 {{ $user }}:www-data {{ $release }}/{{ $file }}
		
		echo "Permissions have been set for {{ $file }}"
    @endforeach
	
	echo "#4 - Permissions has been set"
@endtask

@task('update_symlinks')
    ln -nfs {{ $release }} {{ $current }}
	chgrp -h www-data {{ $current }};

	echo "#5 - Symlinks has been cloned"
@endtask

@macro('deploy', ['on' => 'production'])
	clone
{{--	composer
	artisan
	chmod
	update_symlinks --}}
@endmacro
