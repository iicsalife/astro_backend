-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 08 2019 г., 19:32
-- Версия сервера: 8.0.12
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `astrology`
--

DELIMITER $$
--
-- Процедуры
--
DROP PROCEDURE IF EXISTS `pr_add_article`$$
CREATE PROCEDURE `pr_add_article` (IN `p_author_id` INT, IN `p_category_id` INT, IN `p_slug` VARCHAR(255), IN `p_title` VARCHAR(255), IN `p_content` TEXT, IN `p_photo` VARCHAR(255))  BEGIN
	insert into articles 
    (user_id, category_id,slug,title,content,photo)
    VALUES(p_author_id,p_category_id, p_slug,p_title,p_content,p_photo);
    update users
    set publications = publications+1
    WHERE id = p_author_id;
    select LAST_INSERT_ID() as article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_add_article_comment`$$
CREATE PROCEDURE `pr_add_article_comment` (IN `p_article_id` INT, IN `p_comment_author` INT, IN `p_comment_body` TEXT, IN `p_parent_id` INT)  BEGIN
	insert into article_comments 
    (article_id, comment_author, body, parent_id)
    VALUES(p_article_id, p_comment_author, p_comment_body, p_parent_id);
END$$

DROP PROCEDURE IF EXISTS `pr_add_quiz`$$
CREATE PROCEDURE `pr_add_quiz` (IN `p_category_id` INT, IN `p_title` VARCHAR(255), IN `p_description` TEXT, IN `p_curator_id` INT, IN `p_curator_status` VARCHAR(255), IN `p_discount` INT, IN `p_is_contact` INT, IN `p_quiz_data` JSON)  BEGIN
	insert into quiz
		( category_id, title, description, curator_id, curator_status, discount, is_contact, quiz_data )
	values
		( p_category_id, p_title, p_description, p_curator_id, p_curator_status, p_discount, p_is_contact, p_quiz_data );
		
	select LAST_INSERT_ID() as quiz_id;
END$$

DROP PROCEDURE IF EXISTS `pr_add_tag_article`$$
CREATE PROCEDURE `pr_add_tag_article` (IN `p_article_id` INT, IN `p_tag_id` INT)  BEGIN
	insert into article_tags
    (article_id,tags_id)
    VALUES(p_article_id,p_tag_id);
END$$

DROP PROCEDURE IF EXISTS `pr_add_tag_to_quiz`$$
CREATE PROCEDURE `pr_add_tag_to_quiz` (IN `p_quiz_id` INT, IN `p_tag_id` INT)  BEGIN
	insert into quiz_tags
		( quiz_id, tag_id )
	values
		( p_quiz_id, p_tag_id );
END$$

DROP PROCEDURE IF EXISTS `pr_add_topic`$$
CREATE PROCEDURE `pr_add_topic` (IN `p_user_id` INT, IN `p_category_id` INT, IN `p_slug` VARCHAR(255), IN `p_title` VARCHAR(255), IN `p_body` TEXT)  BEGIN
	insert into topics
		(user_id, category_id, slug, title, body)
	VALUES 
		(p_user_id, p_category_id, p_slug, p_title, p_body);

	select LAST_INSERT_ID() as topic_id;
END$$

DROP PROCEDURE IF EXISTS `pr_add_topic_comment`$$
CREATE PROCEDURE `pr_add_topic_comment` (IN `p_topic_id` INT, IN `p_user_id` INT, IN `p_body` TEXT, IN `p_parent_id` INT)  BEGIN
	insert into topic_comments
		(topic_id, user_id, body, parent_id)
	values
		(p_topic_id, p_user_id, p_body, p_parent_id);
		
	select LAST_INSERT_ID() as topic_comment_id;
END$$

DROP PROCEDURE IF EXISTS `pr_delete_quiz_tags`$$
CREATE PROCEDURE `pr_delete_quiz_tags` (IN `p_quiz_id` INT)  BEGIN
	delete from quiz_tags
	where quiz_id = p_quiz_id;
END$$

DROP PROCEDURE IF EXISTS `pr_del_follower`$$
CREATE PROCEDURE `pr_del_follower` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
	delete from author_follow 
	where author_id = p_author_id and(user_id = p_user_id);
    UPDATE users
    set followers = followers-1
    WHERE id = p_author_id;
END$$

DROP PROCEDURE IF EXISTS `pr_dis_like_article`$$
CREATE PROCEDURE `pr_dis_like_article` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
	delete from article_likes
	where article_id = p_article_id and(user_id = p_user_id);
    UPDATE articles
    set is_like = is_like-1
    WHERE id = p_article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_article`$$
CREATE PROCEDURE `pr_get_article` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
    declare path nvarchar(30)	 default fu_get_sys_param ('AUTHOR_PHOTO_PATH');
    UPDATE articles
    set views_cnt = views_cnt+1
    WHERE id = p_article_id;
	select
		a.id,
		u.id as author,
        c.name as category_name,
        a.slug,
		a.category_id,
		a.title as article_title,
		a.content as article_content,
		a.views_cnt,
		a.is_like as likes_cnt,
		a.is_save ,
		a.created_at,
        a.comment_cnt as article_comment,
        concat(path,u.avatar) as autor_photo,
        a.photo as article_photo,
        group_concat(atg.tags_id) as tag_list,
        (select article_id from article_likes where article_id = p_article_id and(user_id = p_user_id)) as user_is_like
	from articles a
	left join users u on a.user_id = u.id
    left join categories c on a.category_id = c.id
	left join article_tags atg on a.id = atg.article_id
	where a.id = p_article_id
	group by a.id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_article_comment`$$
CREATE PROCEDURE `pr_get_article_comment` (IN `p_article_id` INT)  BEGIN
	declare path nvarchar(30)	 default fu_get_sys_param ('AUTHOR_PHOTO_PATH');
	select
		ac.body,
        ac.parent_id as parent_comment_id,
		u.name as comment_autor,
        concat(path,u.avatar) as autor_photo
	from article_comments ac
	left join users u on ac.comment_author = u.id
	where ac.article_id = p_article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_article_list`$$
CREATE PROCEDURE `pr_get_article_list` (IN `p_category_id` INT, IN `p_tag_list` VARCHAR(255), IN `p_page_num` INT)  BEGIN
	declare l_per_page 		int default fu_get_sys_param ('FORUM_ARTICLE_LIST_CNT');
    declare path nvarchar(30)	 default fu_get_sys_param ('AUTHOR_PHOTO_PATH');
	declare l_offset 			int default (p_page_num * l_per_page);
	select
		a.id,
		u.name as author_name,
        c.name as category_name,
        a.slug,
		a.category_id,
        a.comment_cnt,
		a.title as aricle_title,
		a.content as article_content,
		a.views_cnt,
		a.is_like as likes_cnt,
		a.is_save,
		a.created_at,
        concat(path,u.avatar) as autor_photo,
        group_concat(atg.tags_id) as tag_list
	from articles a
	left join users u on a.user_id = u.id
    left join categories c on a.category_id = c.id
	left join article_tags atg on a.id = atg.article_id
	where a.category_id = p_category_id
 	and (p_tag_list is null or p_tag_list = '0' or FIND_IN_SET(atg.tags_id, p_tag_list))
    group by a.id
	limit l_offset, l_per_page;
END$$

DROP PROCEDURE IF EXISTS `pr_get_author`$$
CREATE PROCEDURE `pr_get_author` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
    declare path nvarchar(30)	 default fu_get_sys_param ('AUTHOR_PHOTO_PATH');
	select
		u.name as author_name,
		u.description as author_description,
        u.followers as author_follow_cnt,
        u.likes as author_like_cnt,
        u.publications as author_public_cnt,
        (select user_id from author_follow where user_id = p_user_id and(author_id = p_author_id)) as user_is_follow,
        concat(path,u.avatar) as autor_avatar
	from users u
	where u.id = p_author_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_curator_list`$$
CREATE PROCEDURE `pr_get_curator_list` ()  BEGIN
	select
		u.id,
		u.name as fio,
		u.email,
		u.position_id,
		up.name as position_name,
		u.avatar
	from users u
	left join user_positions up on u.position_id = up.id
	where u.role_id = 2;
END$$

DROP PROCEDURE IF EXISTS `pr_get_quiz_data`$$
CREATE PROCEDURE `pr_get_quiz_data` (IN `p_quiz_id` INT)  BEGIN
	select
		q.id,
		q.category_id,
		c.name as category_name,
		q.title,
		q.description,
		q.curator_id,
		u.name as curator_name,	
		u.email as curator_email,
		u.avatar as curator_photo,
		q.curator_status,
		u.position_id,
		up.name as position_name,
		q.discount,
		q.is_active,
		q.is_contact,
		q.quiz_data,
		group_concat(qt.tag_id) as tag_list
	from quiz q
	left join users u on q.curator_id = u.id
	left join categories c on q.category_id = c.id
	left join user_positions up on u.position_id = up.id
	left join quiz_tags qt on q.id = qt.quiz_id
	where q.id = p_quiz_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_quiz_list`$$
CREATE PROCEDURE `pr_get_quiz_list` (IN `p_is_draft` INT, IN `p_is_contact` INT)  BEGIN
	select
		q.id,
		q.category_id,
		c.name as category_name,
		q.title,
		q.description,
		q.curator_id,
		u.avatar as curator_photo,
		u.name as curator_name,
		q.is_active,
		group_concat(qt.tag_id) as tag_list
	from quiz q
	left join users u on q.curator_id = u.id
	left join categories c on q.category_id = c.id
	left join quiz_tags qt on q.id = qt.quiz_id
	where ( (p_is_draft = 0 and q.is_active = 1) or (p_is_draft = 1 and q.is_active = 0) )
	and (p_is_contact = 0 or q.is_contact = 1)
	group by q.id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_quiz_user_answers`$$
CREATE PROCEDURE `pr_get_quiz_user_answers` (IN `p_user_id` INT, IN `p_quiz_id` INT)  BEGIN
	select
		q.id as quiz_id,
		q.category_id,
		c.name as category_name,
		q.title,
		q.description,
		q.quiz_data,
		qa.answers as user_answers,
		qa.created
	from quiz_answers qa 
	left join quiz q on qa.quiz_id = q.id
	left join categories c on q.category_id = c.id
	where qa.user_id = p_user_id
	and (p_quiz_id = 0 or q.id = p_quiz_id);
END$$

DROP PROCEDURE IF EXISTS `pr_get_seo_params`$$
CREATE PROCEDURE `pr_get_seo_params` (IN `p_seo_page` VARCHAR(255), IN `p_seo_id` INT)  BEGIN
	SELECT
		seo.title,
		seo.keywords,
		seo.description
	from seo_params seo
	WHERE seo.id = p_seo_id
	AND seo.`page` = p_seo_page;
END$$

DROP PROCEDURE IF EXISTS `pr_get_tags_by_category`$$
CREATE PROCEDURE `pr_get_tags_by_category` (IN `p_category_id` INT)  BEGIN
	select
		t.id,
		t.name,
		t.category_id
	from tags t
	where t.category_id = p_category_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_tag_list`$$
CREATE PROCEDURE `pr_get_tag_list` ()  BEGIN
	select * from tags;
END$$

DROP PROCEDURE IF EXISTS `pr_get_topic`$$
CREATE PROCEDURE `pr_get_topic` (IN `p_topic_id` INT)  BEGIN
	select
		t.id,
		u.id as user_id,
		u.name as user_name,
		t.category_id,
		c.name as category_name,
		t.slug,
		t.title,
		t.body,
		t.follow,
		t.views,
		t.likes,
		t.answers_cnt,
		t.created_at,
		t.updated_at,
		group_concat(tt.tag_id) AS tag_list
	from topics t
	left join users u on t.user_id = u.id
	left join categories c on t.category_id = c.id
	left join topic_tags tt on t.id = tt.topic_id
	WHERE t.id = p_topic_id;
END$$

DROP PROCEDURE IF EXISTS `pr_get_topic_list`$$
CREATE PROCEDURE `pr_get_topic_list` (IN `p_category_id` INT, IN `p_tag_list` VARCHAR(255), IN `p_page_num` INT)  BEGIN
	declare l_per_page 		int default fu_get_sys_param ('FORUM_ARTICLE_LIST_CNT');
	declare l_offset 			int default (p_page_num * l_per_page);

	select
		t.id,
		u.id as user_id,
		u.name as user_name,
		t.category_id,
		c.name as category_name,
		t.slug,
		t.title,
--		t.body,
		t.follow,
		t.views,
		t.likes,
		t.answers_cnt,
		t.created_at
	from topics t
	left join users u on t.user_id = u.id
	left join categories c on t.category_id = c.id
	left join topic_tags tt on t.id = tt.topic_id
	where t.category_id = p_category_id
 	and (p_tag_list is null or p_tag_list = '0' or FIND_IN_SET(tt.tag_id, p_tag_list))
	group by t.id
	limit l_offset, l_per_page;
END$$

DROP PROCEDURE IF EXISTS `pr_get_topic_more_comments`$$
CREATE PROCEDURE `pr_get_topic_more_comments` (IN `p_topic_id` INT, IN `p_page_num` INT)  BEGIN
	declare l_per_page 		int default fu_get_sys_param ('FORUM_MORE_COMMENTS_CNT');
	declare l_offset 			int default (p_page_num * l_per_page);

	select
		tc.id,
		tc.topic_id,
		tc.user_id as author_id,
		u.name as author_name,
		tc.body,
		tc.likes,
		tc.parent_id,
		tc.created_at
	from topic_comments tc
	left join users u on tc.user_id = u.id
	where topic_id = p_topic_id
	order by coalesce(tc.parent_id, tc.id), tc.parent_id is not null, tc.id, created_at
	limit l_offset, l_per_page;
END$$

DROP PROCEDURE IF EXISTS `pr_quiz_set_active`$$
CREATE PROCEDURE `pr_quiz_set_active` (IN `p_quiz_id` INT, IN `p_is_active` INT)  BEGIN
	update quiz
	set is_active = p_is_active
	where id = p_quiz_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_article_like`$$
CREATE PROCEDURE `pr_set_article_like` (IN `p_article_id` INT, IN `p_user_id` INT)  BEGIN
	insert into article_likes 
    (article_id, user_id)
    VALUES(p_article_id, p_user_id);
    UPDATE articles
    set is_like = is_like+1
    WHERE id = p_article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_article_status`$$
CREATE PROCEDURE `pr_set_article_status` (IN `p_article_id` INT, IN `p_is_active` INT)  BEGIN
	update articles
	set 
			is_active = p_is_active
	where id = p_article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_follower`$$
CREATE PROCEDURE `pr_set_follower` (IN `p_author_id` INT, IN `p_user_id` INT)  BEGIN
	insert into author_follow 
    (author_id, user_id)
    VALUES(p_author_id, p_user_id);
    UPDATE users
    set followers = followers+1
    WHERE id = p_author_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_quiz_user_answers`$$
CREATE PROCEDURE `pr_set_quiz_user_answers` (IN `p_user_id` INT, IN `p_quiz_id` INT, IN `p_answers` JSON, IN `p_is_contact` INT, IN `p_user_name` VARCHAR(255), IN `p_user_email` VARCHAR(255), IN `p_user_phone` VARCHAR(255))  BEGIN
	declare l_quiz_answers_id int;
	
	insert into quiz_answers
		( user_id, quiz_id, answers )
	values
		( p_user_id, p_quiz_id, p_answers );

	set l_quiz_answers_id = LAST_INSERT_ID();
	
	if (p_is_contact = 1) then
		insert into quiz_contacts
			( quiz_id, user_id, user_name, user_email, user_phone )
		values
			( p_quiz_id, p_user_id, p_user_name, p_user_email, p_user_phone);
	end if;

	select l_quiz_answers_id as quiz_answers_id;	
END$$

DROP PROCEDURE IF EXISTS `pr_set_seo_params`$$
CREATE PROCEDURE `pr_set_seo_params` (IN `p_id` INT, IN `p_page` VARCHAR(255), IN `p_title` VARCHAR(255), IN `p_description` VARCHAR(255), IN `p_keywords` TEXT)  BEGIN
	insert into seo_params
		(id, `page`, title, description, keywords)
	values
		(p_id, p_page, p_title, p_description, p_keywords);
END$$

DROP PROCEDURE IF EXISTS `pr_set_topic_comment_like`$$
CREATE PROCEDURE `pr_set_topic_comment_like` (IN `p_topic_comment_id` INT, IN `p_user_id` INT, IN `p_like_value` INT)  BEGIN
	if (p_like_value = 1) then
		insert ignore into topic_comment_likes
			(topic_comment_id, user_id)
		values
			(p_topic_comment_id, p_user_id);
	else
		delete from topic_comment_likes
		where topic_comment_id = p_topic_comment_id
		and user_id = p_user_id;
	end if;

	update topic_comments
	set likes = if (p_like_value = 1, likes + 1, likes - 1)
	where id = p_topic_comment_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_topic_follow`$$
CREATE PROCEDURE `pr_set_topic_follow` (IN `p_topic_id` INT, IN `p_user_id` INT, IN `p_follow_value` INT)  BEGIN
	if (p_follow_value = 1) then
		insert into topic_followers
			(topic_id, user_id)
		values
			(p_topic_id, p_user_id);
	else
		delete from topic_followers
		where topic_id = p_topic_id
		and user_id = p_user_id;
	end if;
	
	update topics
	set follow = follow + if(p_follow_value = 1,  1, -1)
	where id = p_topic_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_topic_like`$$
CREATE PROCEDURE `pr_set_topic_like` (IN `p_topic_id` INT, IN `p_user_id` INT, IN `p_like_value` INT)  BEGIN
	if (p_like_value = 1) then
		insert ignore into topic_likes
			(topic_id, user_id)
		values
			(p_topic_id, p_user_id);
	else
		delete from topic_likes
		where topic_id = p_topic_id
		and user_id = p_user_id;
	end if;

	update topics
	set likes = if (p_like_value = 1, likes + 1, likes - 1)
	where id = p_topic_id;
END$$

DROP PROCEDURE IF EXISTS `pr_set_topic_tag`$$
CREATE PROCEDURE `pr_set_topic_tag` (IN `p_topic_id` INT, IN `p_tag_id` INT)  BEGIN
	insert into topic_tags
		(topic_id, tag_id)
	values
		(p_topic_id, p_tag_id);
END$$

DROP PROCEDURE IF EXISTS `pr_set_topic_view`$$
CREATE PROCEDURE `pr_set_topic_view` (IN `p_topic_id` INT)  BEGIN
	update topics
	set views = views + 1
	where id = p_topic_id;
END$$

DROP PROCEDURE IF EXISTS `pr_update_article`$$
CREATE PROCEDURE `pr_update_article` (IN `p_article_id` INT, IN `p_category_id` INT, IN `p_author_id` INT, IN `p_title` VARCHAR(255), IN `p_content` TEXT, IN `p_photo` VARCHAR(255))  BEGIN
	update articles
	set 
			category_id = p_category_id,
			title = p_title,
			content = p_content,
			photo = p_photo,
            user_id = p_author_id
	where id = p_article_id;
END$$

DROP PROCEDURE IF EXISTS `pr_update_quiz`$$
CREATE PROCEDURE `pr_update_quiz` (IN `p_quiz_id` INT, IN `p_category_id` INT, IN `p_title` VARCHAR(255), IN `p_description` TEXT, IN `p_curator_id` INT, IN `p_curator_status` VARCHAR(255), IN `p_discount` INT, IN `p_is_contact` INT, IN `p_quiz_data` JSON)  BEGIN
	update quiz
	set 
			category_id = p_category_id,
			title = p_title,
			description = p_description,
			curator_id = p_curator_id,
			curator_status = p_curator_status,
			discount = p_discount,
			is_contact = p_is_contact,
			quiz_data = p_quiz_data
	where id = p_quiz_id;
END$$

--
-- Функции
--
DROP FUNCTION IF EXISTS `fu_get_sys_param`$$
CREATE FUNCTION `fu_get_sys_param` (`p_param_name` VARCHAR(255)) RETURNS VARCHAR(255) CHARSET utf8 BEGIN
	return (SELECT value from system_params where upper(name) = upper(p_param_name));
END$$

DROP FUNCTION IF EXISTS `fu_is_comments_next`$$
CREATE FUNCTION `fu_is_comments_next` (`p_topic_id` INT, `p_page_num` INT, `p_page_type` VARCHAR(20)) RETURNS INT(11) BEGIN
	declare l_per_page 		int default fu_get_sys_param (concat(upper(p_page_type), '_MORE_COMMENTS_CNT'));
	declare l_offset 			int default (p_page_num * l_per_page);

	return (
		select COUNT(1) > (l_offset + l_per_page) as is_next
		from topic_comments tc
		where topic_id = p_topic_id
	);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `views_cnt` int(11) NOT NULL DEFAULT '0',
  `comment_cnt` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '0',
  `is_like` int(11) NOT NULL DEFAULT '0',
  `is_save` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `category_id`, `slug`, `title`, `content`, `views_cnt`, `comment_cnt`, `is_active`, `is_like`, `is_save`, `photo`, `created_at`, `updated_at`) VALUES
(1, 10, 1, 'slug1', 'test new title', 'text text text text', 3, 0, 1, -1, 0, 'images/articles/0129f0eeaa2c78655b33409e709cb5a8.png', '2019-02-06 20:04:36', '2019-02-06 20:53:32'),
(2, 2, 2, 'slug2', 'Привет из IT', 'Это статья о IT мире', 0, 0, 0, 0, 0, 'images/articles/...', '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(3, 1, 3, 'slug3', 'Привет из IT', 'Это статья о IT мире', 0, 0, 0, 0, 0, 'images/articles/...', '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(4, 2, 2, 'slug4', 'Привет из IT', 'Это статья о IT мире', 1, 0, 0, 0, 0, 'images/articles/...', '2019-02-06 20:04:36', '2019-02-06 20:18:29');

-- --------------------------------------------------------

--
-- Структура таблицы `article_comments`
--

DROP TABLE IF EXISTS `article_comments`;
CREATE TABLE `article_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `comment_author` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `article_comments`
--

INSERT INTO `article_comments` (`id`, `article_id`, `comment_author`, `body`, `parent_id`, `created_at`, `updated_at`) VALUES
(5, 1, 2, 'Хорошая статья!', 0, '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(6, 2, 1, 'Хорошая статья!', 0, '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(7, 1, 3, 'Хорошая статья!', 0, '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(8, 2, 2, 'Хорошая статья!', 0, '2019-02-06 20:04:36', '2019-02-06 20:04:36'),
(9, 1, 1, 'test art comment', 0, '2019-02-06 20:05:03', '2019-02-06 20:05:03'),
(10, 4, 1, 'test art comment', 0, '2019-02-06 20:13:18', '2019-02-06 20:13:18'),
(11, 4, 1, 'test art comment', 10, '2019-02-06 20:15:09', '2019-02-06 20:15:09'),
(12, 4, 1, 'test art comment', 10, '2019-02-06 20:53:18', '2019-02-06 20:53:18');

-- --------------------------------------------------------

--
-- Структура таблицы `article_likes`
--

DROP TABLE IF EXISTS `article_likes`;
CREATE TABLE `article_likes` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `article_likes`
--

INSERT INTO `article_likes` (`article_id`, `user_id`) VALUES
(2, 1),
(3, 1),
(1, 2),
(1, 3),
(4, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `article_tags`
--

DROP TABLE IF EXISTS `article_tags`;
CREATE TABLE `article_tags` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `tags_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `article_tags`
--

INSERT INTO `article_tags` (`article_id`, `tags_id`) VALUES
(1, 1),
(2, 1),
(2, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `author_follow`
--

DROP TABLE IF EXISTS `author_follow`;
CREATE TABLE `author_follow` (
  `author_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `author_follow`
--

INSERT INTO `author_follow` (`author_id`, `user_id`) VALUES
(1, 10),
(2, 10),
(3, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Астрология'),
(2, 'Психология'),
(3, 'Нумерология'),
(4, 'Хиромантия');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(11) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(131, '2014_10_12_100000_create_password_resets_table', 1),
(132, '2019_01_02_214839_create_categories_table', 1),
(133, '2019_01_06_214141_create_topics_table', 1),
(134, '2019_01_08_150530_create_topic_comments_table', 1),
(135, '2019_01_12_092612_create_tags', 1),
(136, '2019_01_13_194427_create_user_roles', 1),
(137, '2019_01_13_194901_create_user_positions', 1),
(138, '2019_01_13_200000_create_users_table', 1),
(139, '2019_01_13_210000_create_quiz_table', 1),
(140, '2019_01_13_220000_create_quiz_questions', 1),
(141, '2019_01_13_230000_create_quiz_answers', 1),
(142, '2019_01_13_240000_create_quiz_user_answers', 1),
(515, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(516, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(517, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(518, '2016_06_01_000004_create_oauth_clients_table', 2),
(519, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(520, '2019_01_29_091326_create_permission_tables', 2),
(521, '2019_01_30_130534_create_articles_table', 2),
(522, '2019_01_30_132156_create_article_comments_table', 2),
(523, '2019_01_30_163152_create_article_tags_table', 2),
(524, '2019_01_31_194138_blog_procedures', 2),
(525, '2019_02_04_141314_create_article_likes_table', 2),
(526, '2019_02_06_090907_update_user_table', 2),
(527, '2019_02_06_103625_create_author_follow_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 10),
(24, 'App\\User', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('06763466c07f08510fcb9d0f16b76f28dc157f39cd0d0293e222aad8f002714ca8a9056a033a6e91', 13, 1, 'Personal Access Token', '[]', 0, '2019-02-08 09:40:18', '2019-02-08 09:40:18', '2020-02-08 11:40:18'),
('13d1fade359c896e7c89b2b4341c3452ecb6dbbea50e0972a4a49e7f6799a0e578aecc83bb6a5ba9', 10, 1, 'Personal Access Token', '[]', 0, '2019-01-29 04:06:27', '2019-01-29 04:06:27', '2019-02-05 09:06:27'),
('144191f780d6a03390c2e4b6fbf4330b83de43782543e585d8dbe02ffc90ec24c500430fa3c6ab8c', 10, 1, 'Personal Access Token', '[]', 1, '2019-01-29 04:01:36', '2019-01-29 04:01:36', '2019-02-05 09:01:37'),
('3f69cc97fe2d185f207f606ebcdd438719d9dc8458873e3d5a36df9543846f07f0667c700d4b4164', 10, 1, 'Personal Access Token', '[]', 1, '2019-02-08 09:42:37', '2019-02-08 09:42:37', '2020-02-08 11:42:37'),
('70ca4c2bf7ad426d49fda2c9cdf917672acdbc1791e10688ee213a4c3146ea449696c913c893e1b2', 10, 1, 'Personal Access Token', '[]', 0, '2019-02-01 15:18:09', '2019-02-01 15:18:09', '2020-02-01 19:18:09'),
('7a7f213d923935c0e11dbd9961402684bc7a8c27959eb62fe837a8a8c7b850ff8c9cb247bbc00bb6', 10, 1, 'Personal Access Token', '[]', 0, '2019-02-08 10:00:47', '2019-02-08 10:00:47', '2020-02-08 12:00:47'),
('7ff80dc02097c2fe13933eff33b5b6f41530f9fe6cd704d06889e2eb88ef66423bdb84c4bd62d4a8', 13, 1, 'Personal Access Token', '[]', 0, '2019-02-08 09:33:24', '2019-02-08 09:33:24', '2020-02-08 11:33:24'),
('8c0f73e3d8d003c0f4bedb167a5ac5f86d0db73add4db21e8af017bd8fc672851c0c5a79fc78e4d1', 11, 1, 'Personal Access Token', '[]', 0, '2019-02-08 08:47:14', '2019-02-08 08:47:14', '2020-02-08 10:47:14'),
('9f61b0d267fa196adba088d5a28a32e85bf2d15fd4a0bea4987b72f33438e73fa6bbac583925456c', 13, 1, 'Personal Access Token', '[]', 0, '2019-02-08 09:23:42', '2019-02-08 09:23:42', '2020-02-08 11:23:42'),
('c0bf6d03e0f4607680d59bafd9ce808ce06509b9bf1bd42782fef6e46702a25b1c6a49c03eaab0b7', 13, 1, 'Personal Access Token', '[]', 0, '2019-02-08 09:37:21', '2019-02-08 09:37:21', '2020-02-08 11:37:21'),
('c40da98ea59c38504d7a612090073a76ab620b7fe3da7af739728d0b113a446400842a42e3fad5c6', 10, 1, 'Personal Access Token', '[]', 0, '2019-02-01 15:54:39', '2019-02-01 15:54:39', '2020-02-01 19:54:39'),
('e43b93ddaf81e4387570fcd4f32cb98da4f427f883700bc7ea6568e6b0396cde32847e686d16a0bf', 10, 1, 'Personal Access Token', '[]', 0, '2019-02-08 13:38:34', '2019-02-08 13:38:34', '2020-02-08 15:38:34');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ZiwpbuJjItQISyUqcnQHt5RvYG3qcqiRvA0kyWga', 'http://localhost', 1, 0, 0, '2019-01-29 03:37:03', '2019-01-29 03:37:03'),
(2, NULL, 'Laravel Password Grant Client', 'TtpO8XafDm8uTDeUzfRQvdTOFR7Ou6EL7ur9uwF8', 'http://localhost', 0, 1, 0, '2019-01-29 03:37:03', '2019-01-29 03:37:03');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-01-29 03:37:03', '2019-01-29 03:37:03');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(2, 'test_priv', 'api', '2019-02-08 09:27:10', '2019-02-08 09:27:10'),
(5, 'test_priv2', 'api', '2019-02-08 14:16:03', '2019-02-08 14:16:03'),
(6, 'test_priv3', 'api', '2019-02-08 14:16:13', '2019-02-08 14:16:13');

-- --------------------------------------------------------

--
-- Структура таблицы `quiz`
--

DROP TABLE IF EXISTS `quiz`;
CREATE TABLE `quiz` (
  `id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `curator_id` int(11) UNSIGNED NOT NULL,
  `curator_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` int(11) UNSIGNED DEFAULT NULL,
  `quiz_data` json NOT NULL,
  `is_contact` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `quiz`
--

INSERT INTO `quiz` (`id`, `category_id`, `title`, `description`, `curator_id`, `curator_status`, `discount`, `quiz_data`, `is_contact`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Тест на уровень', 'Добрейшего времени суток, милый гость :)<br>Спасибо, что вы здесь. Уверены, вам понравится то, что вам предстоит. Что это?<br>Это тест. Он сначала узнаёт о вашей компании, а потом мы предлагаем решение ваших задач по маркетингу.', 1, NULL, 500, 'null', 0, 1, '2019-01-14 09:22:07', '2019-01-14 09:22:07'),
(2, 2, 'test quiz2', 'desc for test quiz222', 3, NULL, 4002, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 1, 1, '2019-01-14 09:39:15', '2019-01-21 23:30:28'),
(3, 2, 'test quiz 2', 'desc for test quiz 2', 2, NULL, 600, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 0, 1, '2019-01-14 09:42:03', '2019-01-14 09:55:45'),
(4, 2, 'test quiz 3', '222', 3, NULL, 222, '{\"pages\": [{\"name\": \"Страница 1\", \"elements\": [{\"name\": \"Вопрос 1\", \"type\": \"checkbox\", \"choices\": [{\"text\": \"ццц\", \"value\": \"item1\"}, {\"text\": \"item22222\", \"value\": \"item2\"}, {\"text\": \"3333\", \"value\": \"item3\"}], \"correctAnswer\": [\"item2\", \"item1\"]}]}]}', 0, 1, '2019-01-14 09:43:37', '2019-01-21 23:36:11'),
(5, 1, 'test quiz 4', 'desc for test quiz 4', 7, NULL, 200, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 0, 1, '2019-01-14 09:43:37', '2019-01-14 09:55:38'),
(6, 3, 'test quiz 4', 'desc for test quiz 4', 7, NULL, 200, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 0, 1, '2019-01-14 09:43:37', '2019-01-14 09:55:38'),
(7, 4, 'test quiz 4', 'desc for test quiz 4', 8, NULL, 480, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 0, 1, '2019-01-14 09:43:37', '2019-01-14 09:56:01'),
(8, 4, 'test quiz 4', 'desc for test quiz 4', 9, NULL, 620, '{\"test\": \"yes\", \"param\": 1, \"is_active\": true}', 0, 1, '2019-01-14 09:43:37', '2019-01-14 09:56:04'),
(9, 1, 'test quiz', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1111\", \"param\": 3, \"is_active\": false}', 0, 0, '2019-01-14 09:59:56', '2019-01-19 14:43:16'),
(10, 1, 'test qui 2222z', 'desc for test quiz444', 1, NULL, 400, '{\"test\": \"yes\", \"param\": 1, \"is_fff\": false}', 0, 1, '2019-01-14 10:02:23', '2019-01-19 14:43:10'),
(11, 2, 'test qui 22222222222', 'desc', 8, NULL, 200, '{\"test\": \"NO\", \"param\": 1, \"is_fff\": false}', 0, 1, '2019-01-14 19:16:38', '2019-01-14 19:17:13'),
(12, 3, '99999', '33333', 3, NULL, 333, '{\"test\": \"3333\", \"param\": 1, \"is_fff\": false}', 0, 1, '2019-01-16 08:00:13', '2019-01-16 20:12:18'),
(13, 2, 'test quiz', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 19:48:03', '2019-01-16 19:48:03'),
(14, 3, '3333', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 19:52:02', '2019-01-16 19:52:02'),
(15, 3, '9999921', '33333', 3, NULL, 333, '{\"test\": \"3333\", \"param\": 1, \"is_fff\": false}', 0, 1, '2019-01-16 19:53:24', '2019-01-16 20:31:17'),
(16, 3, '3333', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 19:53:45', '2019-01-16 19:53:45'),
(17, 3, '777', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 20:08:55', '2019-01-16 20:08:55'),
(18, 3, '777', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 20:09:26', '2019-01-16 20:09:26'),
(19, 3, '777', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"1222111\", \"param\": 3, \"is_active\": false}', 0, 1, '2019-01-16 20:20:44', '2019-01-16 20:20:44'),
(21, 3, '9999921', '33333', 3, NULL, 333, '{\"test\": \"111\"}', 0, 1, '2019-01-16 20:33:57', '2019-01-17 16:51:05'),
(22, 3, '890', 'desc for test quiz', 1, NULL, 400, '{\"www\": \"555\", \"param\": 3, \"is_active\": false}', 1, 1, '2019-01-17 19:54:04', '2019-01-17 19:54:04'),
(23, 3, '9999921', '33333', 3, 'new status', 333, '{\"test\": \"111\"}', 1, 1, '2019-01-18 20:55:24', '2019-01-21 16:48:05'),
(24, 1, 'Test', 'qwqweqw', 7, NULL, 400, '{\"pages\": [{\"name\": \"Страница 1\", \"elements\": [{\"name\": \"Вопрос 1\", \"type\": \"checkbox\", \"choices\": [\"Вариант1\", \"Вариант2\", \"Вариант3\"], \"correctAnswer\": [\"Вариант2\", \"Вариант3\"]}]}, {\"name\": \"Страница 2\", \"elements\": [{\"name\": \"Вопрос 2\", \"type\": \"checkbox\", \"choices\": [\"item1\", \"item2\", \"item3\"], \"correctAnswer\": [\"item1\"]}]}], \"locale\": \"ru\"}', 0, 1, '2019-01-30 06:23:08', '2019-01-30 06:39:54'),
(25, 1, 'Тестовый опрос 1', 'уууу', 3, NULL, 1200, '{\"pages\": [{\"name\": \"Страница 1\", \"elements\": [{\"name\": \"Вопрос 1\", \"type\": \"checkbox\", \"choices\": [\"Вариант1\", \"Вариант2\", \"Вариант3\"], \"correctAnswer\": [\"Вариант2\", \"Вариант3\"]}]}, {\"name\": \"Страница 2\", \"elements\": [{\"name\": \"Вопрос 2\", \"type\": \"checkbox\", \"choices\": [\"Вариант1\", \"Вариант2\", \"Вариант3\"], \"correctAnswer\": [\"Вариант1\"]}]}]}', 0, 1, '2019-01-30 06:41:21', '2019-01-30 06:41:21'),
(26, 1, 'TEST Quiz', 'rrrrr', 9, NULL, 1200, '{\"pages\": [{\"name\": \"Страница 1\", \"elements\": [{\"name\": \"Вопрос 1\", \"type\": \"checkbox\", \"choices\": [\"Вариант1\", \"Вариант2\", \"Вариант3\"], \"correctAnswer\": [\"Вариант2\", \"Вариант3\"]}]}, {\"name\": \"Страница 2\", \"elements\": [{\"name\": \"Вопрос 2\", \"type\": \"checkbox\", \"choices\": [\"Вариант1\", \"Вариант2\", \"Вариант3\"], \"correctAnswer\": [\"Вариант1\"]}]}]}', 0, 1, '2019-01-30 06:43:03', '2019-01-30 06:43:30');

-- --------------------------------------------------------

--
-- Структура таблицы `quiz_answers`
--

DROP TABLE IF EXISTS `quiz_answers`;
CREATE TABLE `quiz_answers` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) UNSIGNED NOT NULL,
  `answers` json NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `quiz_answers`
--

INSERT INTO `quiz_answers` (`id`, `user_id`, `quiz_id`, `answers`, `created`) VALUES
(58, 1, 2, '{\"quiz_id\": 2, \"quiz_answer_id\": 58}', '2019-01-16 07:52:31'),
(59, 3, 3, '{\"quiz_id\": 3, \"quiz_answer_id\": 59}', '2019-01-16 07:52:31'),
(60, 3, 4, '{\"quiz_id\": 4, \"quiz_answer_id\": 60}', '2019-01-16 07:52:31'),
(61, 2, 6, '{\"quiz_id\": 6, \"quiz_answer_id\": 61}', '2019-01-16 07:52:31'),
(62, 2, 6, '{\"quiz_id\": 6, \"quiz_answer_id\": 62}', '2019-01-16 07:52:31'),
(63, 4, 2, '{\"quiz_id\": 2, \"quiz_answer_id\": 63}', '2019-01-16 07:52:31'),
(64, 5, 2, '{\"quiz_id\": 2, \"quiz_answer_id\": 64}', '2019-01-16 07:52:31'),
(65, 6, 7, '{\"quiz_id\": 7, \"quiz_answer_id\": 65}', '2019-01-16 07:52:31'),
(66, 2, 3, '{\"quiz_id\": 3, \"quiz_answer_id\": 66}', '2019-01-16 07:52:31'),
(67, 2, 6, '{\"quiz_id\": 6, \"quiz_answer_id\": 67}', '2019-01-16 07:52:31'),
(68, 2, 11, '{\"quiz_id\": 11, \"quiz_answer_id\": 68}', '2019-01-16 07:52:31'),
(69, 2, 10, '{\"quiz_id\": 10, \"quiz_answer_id\": 69}', '2019-01-16 07:53:50'),
(70, 2, 10, '{\"test_data\": \"test\"}', '2019-01-17 19:28:58'),
(73, 2, 10, '{\"test_data\": \"test\"}', '2019-01-17 19:33:07'),
(74, 2, 22, '{\"test_data\": \"test\"}', '2019-01-17 19:54:18'),
(75, 1, 4, '{\"Вопрос 1\": [\"item3\"]}', '2019-01-21 23:36:18'),
(76, 1, 24, '{\"Вопрос 1\": [\"item2\", \"item3\"]}', '2019-01-30 06:23:51'),
(77, 1, 24, '{\"Вопрос 1\": [\"item2\"]}', '2019-01-30 06:25:01'),
(78, 1, 24, '{\"Вопрос 1\": [\"item2\", \"item3\"], \"Вопрос 2\": \"item2\"}', '2019-01-30 06:26:24'),
(79, 1, 24, '{\"Вопрос 2\": \"item2\", \"Вопрос 3\": [\"item2\", \"item1\"]}', '2019-01-30 06:27:48'),
(80, 1, 24, '{\"Вопрос 1\": [\"item1\"], \"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:32:50'),
(81, 1, 24, '{\"Вопрос 1\": [\"item2\", \"item3\"], \"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:33:10'),
(82, 1, 24, '{\"Вопрос 1\": [\"item2\", \"item3\"], \"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:33:55'),
(83, 1, 24, '{\"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:38:31'),
(84, 1, 24, '{\"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:39:26'),
(85, 1, 24, '{\"Вопрос 2\": [\"item1\"]}', '2019-01-30 06:40:03'),
(86, 1, 25, '{\"Вопрос 1\": [\"Вариант2\", \"Вариант3\"], \"Вопрос 2\": [\"Вариант1\"]}', '2019-01-30 06:41:30'),
(87, 1, 25, '{\"Вопрос 1\": [\"Вариант1\"], \"Вопрос 2\": [\"Вариант2\", \"Вариант3\"]}', '2019-01-30 06:42:15'),
(88, 1, 26, '{\"Вопрос 1\": [\"Вариант2\", \"Вариант3\"], \"Вопрос 2\": [\"Вариант1\"]}', '2019-01-30 06:43:37'),
(89, 1, 26, '{\"Вопрос 1\": [\"Вариант1\"], \"Вопрос 2\": [\"Вариант3\"]}', '2019-01-30 06:44:01');

-- --------------------------------------------------------

--
-- Структура таблицы `quiz_contacts`
--

DROP TABLE IF EXISTS `quiz_contacts`;
CREATE TABLE `quiz_contacts` (
  `quiz_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `quiz_contacts`
--

INSERT INTO `quiz_contacts` (`quiz_id`, `user_id`, `user_name`, `user_email`, `user_phone`) VALUES
(10, 2, 'alex', 'alex@rr.tt', '12345678'),
(22, 2, 'alex', 'alex@rr.tt', '12345678');

-- --------------------------------------------------------

--
-- Структура таблицы `quiz_tags`
--

DROP TABLE IF EXISTS `quiz_tags`;
CREATE TABLE `quiz_tags` (
  `quiz_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `quiz_tags`
--

INSERT INTO `quiz_tags` (`quiz_id`, `tag_id`) VALUES
(1, 1),
(24, 1),
(1, 2),
(25, 2),
(26, 2),
(1, 4),
(24, 4),
(2, 5),
(2, 6),
(4, 6),
(2, 7),
(21, 8),
(23, 8),
(16, 9),
(17, 9),
(18, 9),
(19, 9),
(22, 9),
(15, 10),
(21, 10),
(23, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super_admin', 'api', '2019-01-29 04:42:04', '2019-01-29 04:42:04'),
(4, 'admin', 'api', '2019-01-29 05:21:07', '2019-01-29 05:21:07'),
(5, 'manager', 'api', '2019-01-29 05:21:29', '2019-01-29 05:21:29'),
(6, 'curator', 'api', '2019-01-29 05:21:46', '2019-01-29 05:21:46'),
(7, 'student', 'api', '2019-01-29 05:21:56', '2019-01-29 05:21:56'),
(8, 'user', 'api', '2019-01-29 05:22:07', '2019-01-29 05:22:07'),
(24, 'test_role', 'api', '2019-02-08 09:27:02', '2019-02-08 09:27:02');

-- --------------------------------------------------------

--
-- Структура таблицы `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `seo_params`
--

DROP TABLE IF EXISTS `seo_params`;
CREATE TABLE `seo_params` (
  `id` int(11) UNSIGNED NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `description` varchar(400) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `seo_params`
--

INSERT INTO `seo_params` (`id`, `page`, `title`, `keywords`, `description`) VALUES
(0, 'blog', 'Блог', 'блог,статьи,комментарии,автора', 'Наш блог по Астрологии'),
(0, 'course', 'Курсы', 'изучение,уроки,астрология,психология,нумерология,курсы', 'Изучай Астрологию'),
(0, 'forum', 'Форум', 'форум,вопросы,ответы,комментарии', 'Наш форум по астрологии'),
(0, 'home', 'Ведическая астрология бесплатно', NULL, 'Ведическая Астрология обучение для начинающих. Сертифицированные преподаватели. Эксперты из Индии. Проф.система подготовки. В группах/ индив-но/Кураторы/Практика/Сертификат. '),
(0, 'quiz', 'Тесты', 'пройти тест,астрология,психология', 'Определи свои знания проходя тесты'),
(10, 'forum', 'topic_10', 'keys keys keys keys ', 'desc desc desc desc desc desc'),
(42, 'forum', 'Название топика', '', ''),
(43, 'forum', 'Название топика', 'test seo keys', 'test seo title'),
(44, 'forum', 'Название топика', '', ''),
(45, 'forum', 'цукцукцу', '', ''),
(46, 'forum', 'вапвап', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `system_params`
--

DROP TABLE IF EXISTS `system_params`;
CREATE TABLE `system_params` (
  `id` smallint(4) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `system_params`
--

INSERT INTO `system_params` (`id`, `name`, `value`, `description`) VALUES
(1, 'BLOG_ARTICLE_LIST_CNT', '10', 'Количество статей в списке на одной странице блога'),
(2, 'FORUM_ARTICLE_LIST_CNT', '10', 'Количество статей в списке на одной странице форума'),
(3, 'BLOG_MORE_COMMENTS_CNT', '5', 'Блог: количество подгружаемых комментариев по кнопке Отобразить еще'),
(4, 'FORUM_MORE_COMMENTS_CNT', '2', 'Форум: количество подгружаемых комментариев по кнопке Отобразить еще');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `name`, `category_id`) VALUES
(1, 'Дома', 1),
(2, 'Планеты', 1),
(3, 'Солнце', 1),
(4, 'Земля', 1),
(5, 'Желание', 2),
(6, 'Методика', 2),
(7, 'Жизнь', 2),
(8, 'Аура', 3),
(9, 'Цифра', 3),
(10, 'отклик', 3),
(11, 'Ладонь', 4),
(12, 'Линии', 4),
(13, 'Пальцы', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `topics`
--

DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `follow` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `views` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `likes` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `answers_cnt` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `topics`
--

INSERT INTO `topics` (`id`, `user_id`, `category_id`, `slug`, `title`, `body`, `follow`, `views`, `likes`, `answers_cnt`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'slug1', '1 Профессиональное образование в области Астрология', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 8, 5, 0, 0, '2019-01-14 09:22:07', '2019-02-06 07:18:06'),
(2, 1, 2, 'slug2', '2 Профессиональное образование в области Психология', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 45, 14, 7, 0, '2019-01-14 09:22:07', '2019-02-05 16:41:05'),
(3, 1, 3, 'slug3', '3 Профессиональное образование в области Нумерология', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 22, 12, 5, 3, '2019-01-14 09:22:07', '2019-02-04 15:22:12'),
(4, 1, 4, 'slug4', '4 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 11, 8, 6, 4, '2019-01-14 09:22:07', '2019-02-04 15:22:15'),
(5, 1, 4, 'slug5', '5 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 7, 18, 4, 6, '2019-01-14 09:22:07', '2019-02-04 15:22:17'),
(6, 1, 2, 'slug6', '6 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 6, 3, 1, 3, '2019-01-14 09:22:07', '2019-02-04 15:22:19'),
(7, 1, 3, 'slug7', '7 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 4, 122, 5, 77, '2019-01-14 09:22:07', '2019-02-04 15:22:21'),
(8, 1, 1, 'slug8', '8 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 2, 11, 23, 8, '2019-01-14 09:22:07', '2019-02-04 15:22:25'),
(9, 1, 1, 'slug9', '9 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 3, 18, 2, 9, '2019-01-14 09:22:07', '2019-02-04 15:22:27'),
(10, 1, 1, 'slug10', '10 Профессиональное образование в области Хиромантия', 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 15, 31, 10, 6, '2019-01-14 09:22:07', '2019-02-06 15:36:50'),
(42, 10, 1, 'nazvanie-topika', 'Название топика', '<p>test desc</p>', 0, 0, 0, 0, '2019-02-05 14:56:50', '2019-02-05 14:56:50'),
(43, 10, 1, 'nazvanie-topika-1', 'Название топика', '<p>test desc</p>', 0, 0, 0, 0, '2019-02-05 14:57:10', '2019-02-05 14:57:10'),
(44, 10, 1, 'nazvanie-topika-2', 'Название топика', '<p>test desc</p>', 0, 0, 0, 0, '2019-02-05 14:59:26', '2019-02-05 14:59:26'),
(45, 10, 1, 'cukcukcu', 'цукцукцу', '<p>цукцукцукцук</p>', 0, 0, 0, 0, '2019-02-05 17:06:28', '2019-02-05 17:06:28'),
(46, 10, 1, 'vapvap', 'вапвап', '<p><u>теар</u>сывфв фывыф</p><p>фывжлоаыва</p><p>ыва</p><p>ыва</p><p>ыва</p><p>ыва</p><p>ываыаыва</p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p>ываыва</p><p>ыва</p><p>ы</p><p>ва</p><p>ыв</p><p>аываыва</p>', 0, 0, 0, 0, '2019-02-05 17:21:00', '2019-02-05 17:21:00');

-- --------------------------------------------------------

--
-- Структура таблицы `topic_comments`
--

DROP TABLE IF EXISTS `topic_comments`;
CREATE TABLE `topic_comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `topic_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `likes` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `topic_comments`
--

INSERT INTO `topic_comments` (`id`, `topic_id`, `user_id`, `body`, `parent_id`, `likes`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 3, '2019-01-14 09:22:07', '2019-02-05 17:25:29'),
(2, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 2, '2019-01-14 09:22:07', '2019-02-05 17:03:11'),
(3, 2, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 3, '2019-01-14 09:22:07', '2019-02-05 16:34:45'),
(4, 2, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 3, 3, '2019-01-14 09:22:07', '2019-02-05 16:34:46'),
(5, 2, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(6, 4, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(7, 4, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 6, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(8, 4, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 6, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(9, 4, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(10, 5, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(11, 5, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(12, 6, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(13, 10, 1, '13Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 2, '2019-01-14 09:22:07', '2019-02-06 12:09:50'),
(14, 10, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 13, 4, '2019-01-14 09:22:07', '2019-02-05 12:09:38'),
(15, 10, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 13, 0, '2019-01-14 09:22:07', '2019-02-01 22:04:56'),
(16, 10, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 3, '2019-01-14 09:22:07', '2019-02-05 11:48:53'),
(17, 6, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(18, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 2, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(19, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 2, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(20, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(21, 2, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(22, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(23, 1, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', 2, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(26, 10, 10, 'test comment', NULL, 1, '2019-02-04 12:38:36', '2019-02-05 12:09:40'),
(27, 10, 10, 'test comment', 26, 0, '2019-02-04 12:38:44', '2019-02-04 12:38:44'),
(28, 2, 1, 'Разные школы астрологии считают, что планеты влияют на людей тяготением, приливными силами или магнетизмом. Но ведь даже студент-первокурсник может рассчитать величину этих сил. И такие расчеты, конечно, есть. Они показывают, что акушер, принимающий ребенка, оказывает на него гравитационное воздействие в шесть раз более сильное, а приливное действие в два триллиона раз более сильное, чем Марс. Масса врача несоизмеримо меньше, чем планеты, но он гораздо ближе к ребенку.', NULL, 0, '2019-01-14 09:22:07', '2019-01-15 13:42:42'),
(29, 2, 10, '<p>фффффффффффффффффффффффффффффффф</p>', NULL, 0, '2019-02-05 16:37:58', '2019-02-05 16:37:58'),
(30, 2, 10, '<p>шшшшшшшшшшшшшшшшшшшшш</p>', 29, 2, '2019-02-05 16:38:24', '2019-02-05 16:38:40'),
(31, 1, 10, '<p>аааааааааааааааааа</p>', NULL, 0, '2019-02-05 17:08:50', '2019-02-05 17:08:50'),
(32, 8, 10, '<p>ыыыыыыыыыыыыыыыыыы</p>', NULL, 0, '2019-02-05 17:16:56', '2019-02-05 17:16:56'),
(33, 8, 10, '<p>ыыыыыыыыыыыыыыыыыы44444444ен</p>', NULL, 0, '2019-02-05 17:17:32', '2019-02-05 17:17:32'),
(34, 1, 10, '<p>цццццццццццццццц</p>', NULL, 0, '2019-02-05 17:22:40', '2019-02-05 17:22:40'),
(35, 1, 10, '<p>цццццццццццц66666666666гнннттттттцццц</p>', 1, 0, '2019-02-05 17:24:06', '2019-02-05 17:24:06'),
(36, 1, 10, '<p>дддддддддддддддддддддддддддд</p>', 31, 0, '2019-02-05 17:29:33', '2019-02-05 17:29:33'),
(37, 1, 10, '<p>12345</p>', 1, 0, '2019-02-05 17:30:28', '2019-02-05 17:30:28'),
(38, 6, 10, '<p>аааааааааааааааааааааа</p>', NULL, 0, '2019-02-05 17:35:58', '2019-02-05 17:35:58'),
(39, 6, 10, '<p>7777777777777777777</p>', 38, 0, '2019-02-05 17:36:15', '2019-02-05 17:36:15'),
(40, 6, 10, '<p>зззззззззззззззззззз</p>', NULL, 0, '2019-02-05 17:36:28', '2019-02-05 17:36:28'),
(41, 10, 10, '<p>ыыыыыыыыыыыыыыыыыыыыыыыыыы</p>', 16, 0, '2019-02-06 12:06:13', '2019-02-06 12:06:13'),
(42, 10, 10, '<p>ййййй</p>', 13, 0, '2019-02-06 12:08:21', '2019-02-06 12:08:21'),
(43, 10, 10, '<p>фффффффффффффффффффффффффффф</p>', NULL, 0, '2019-02-06 15:35:21', '2019-02-06 15:35:21'),
(44, 10, 10, '<p>ффффффффффффппппппппппппппппппфффффффффффффффф</p>', 26, 0, '2019-02-06 15:35:36', '2019-02-06 15:35:36'),
(45, 10, 10, '<p>вввввввввввввввввввв</p>', 13, 0, '2019-02-06 15:36:26', '2019-02-06 15:36:26');

-- --------------------------------------------------------

--
-- Структура таблицы `topic_comment_likes`
--

DROP TABLE IF EXISTS `topic_comment_likes`;
CREATE TABLE `topic_comment_likes` (
  `topic_comment_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `topic_comment_likes`
--

INSERT INTO `topic_comment_likes` (`topic_comment_id`, `user_id`) VALUES
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(13, 10),
(14, 10),
(26, 10),
(30, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `topic_followers`
--

DROP TABLE IF EXISTS `topic_followers`;
CREATE TABLE `topic_followers` (
  `topic_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `topic_likes`
--

DROP TABLE IF EXISTS `topic_likes`;
CREATE TABLE `topic_likes` (
  `topic_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `topic_tags`
--

DROP TABLE IF EXISTS `topic_tags`;
CREATE TABLE `topic_tags` (
  `topic_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `topic_tags`
--

INSERT INTO `topic_tags` (`topic_id`, `tag_id`) VALUES
(1, 1),
(8, 3),
(9, 2),
(46, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `position_id` int(11) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `followers` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `publications` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `position_id`, `status`, `avatar`, `password`, `remember_token`, `description`, `followers`, `likes`, `publications`, `created_at`, `updated_at`) VALUES
(1, 4, 'admin', 'admin@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc1', 2, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(2, 2, 'Валерий Сергеев', 'admin2@ukr.net', NULL, 4, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc2', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(3, 2, 'Виталий Алчев', 'admin3@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc3', 1, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(4, 3, 'Анна Рыбина', 'admin4@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc4', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(5, 1, 'admin', 'admin5@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc5', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(6, 1, 'admin', 'admin6@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc6', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(7, 2, 'Сергей Потапов', 'admin7@ukr.net', NULL, 3, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc7', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(8, 2, 'Игорь Залецкий', 'admin8@ukr.net', NULL, 2, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc8', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(9, 2, 'Станислав Жаров', 'admin9@ukr.net', NULL, 1, 1, 'ava.png', '$2y$10$gdKCpoG8afufCB54LbbvdeavnyPAoFMb0AAo6iMs0g7oqSSORn9D6', 'Zdss8Y4VSl3dQJA31AQmHEz73UjUcgIhIPvUaazewfl5b2rmtXNb4B1nN6CG', 'desc9', 0, 0, 0, '2018-10-02 11:00:04', '2018-10-02 11:00:04'),
(10, 6, 'Super Admin', 'superadmin@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$6z2BU/3vWCcdhjm2Xn4RR.QRGWD.0gTxruJVYItC2NnslyY6LV/RS', NULL, 'desc10', 0, 0, 0, '2019-01-29 06:00:45', '2019-01-29 06:00:45'),
(11, NULL, 'test_admin', 'admin@test.test', NULL, NULL, NULL, NULL, '$2y$10$sNBkb4OYcHx/XPd.4EyE/.dar7B2ZYXNmyttfdaY961Y2ePykJcMm', NULL, NULL, 0, 0, 0, '2019-02-08 08:42:08', '2019-02-08 08:42:08');

-- --------------------------------------------------------

--
-- Структура таблицы `user_positions`
--

DROP TABLE IF EXISTS `user_positions`;
CREATE TABLE `user_positions` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_positions`
--

INSERT INTO `user_positions` (`id`, `name`) VALUES
(1, 'Астролог'),
(2, 'Психолог'),
(3, 'Нумеролог'),
(4, 'Хиромант');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`) VALUES
(1, 'Гость'),
(2, 'Студент'),
(3, 'Куратор'),
(4, 'Менеджер'),
(5, 'Администратор'),
(6, 'Супер администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `_quiz_answers_old`
--

DROP TABLE IF EXISTS `_quiz_answers_old`;
CREATE TABLE `_quiz_answers_old` (
  `id` int(11) UNSIGNED NOT NULL,
  `question_id` int(11) UNSIGNED NOT NULL,
  `answer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_correct` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `_quiz_answers_old`
--

INSERT INTO `_quiz_answers_old` (`id`, `question_id`, `answer`, `answer_type`, `is_correct`) VALUES
(1, 1, '0', '', 1),
(6, 2, '121  нет', '', 1),
(7, 3, 'Седьмой', '', 0),
(8, 3, 'Двенадцатый', '', 0),
(9, 3, 'Девятый', '', 0),
(10, 3, 'Первый', '', 1),
(11, 4, 'В том доме, где есть вода', '', 0),
(12, 4, 'В доме, отвечающем за интуицию', '', 0),
(13, 4, 'В любом доме, так как знаки зодиака перемещаются', '', 1),
(14, 4, 'В доме, противоположном доме личности', '', 0),
(15, 5, 'Лев', '', 0),
(16, 5, 'Скорпион', '', 0),
(17, 5, 'Стрелец', '', 0),
(18, 5, 'Рыбы', '', 1),
(19, 5, 'Дева', '', 0),
(20, 6, 'Дхармы', '', 0),
(21, 6, 'Кендры', '', 0),
(22, 6, 'Упачаий', '', 1),
(23, 7, 'Солнце, символизирующее знания', '', 0),
(24, 7, 'Луна, символизирующая эмоции', '', 1),
(25, 7, 'Сатурн, указывающий на сердечные страдания', '', 0),
(26, 7, 'Венера, указывающая на супруга/супругу', '', 0),
(27, 7, 'Венера, указывающая на средства передвижения', '', 1),
(28, 7, 'Меркурий, символизирующий внутреннюю интуицию', '', 0),
(29, 7, 'Марс, указывающий на недвижимость', '', 1),
(30, 8, 'Шравана, Магха , Мула', '', 0),
(31, 8, 'Ардра, Рохини, Шатабхиша', '', 0),
(32, 8, 'Рохини, Хаста, Шравани', '', 1),
(33, 8, 'Мригашира, Хаста, Шравани', '', 0),
(34, 9, 'Учиться выносить трудности, не причинять страданий другим, развивать оптимизм', '', 1),
(35, 9, 'Учиться состраданию, учиться не обижаться', '', 1),
(36, 9, 'Развивать уважение к учителю, партнеру, взглядам других', '', 0),
(37, 9, 'Заниматься физическим трудом', '', 1),
(38, 10, 'Марс', '', 0),
(39, 10, 'Меркурий', '', 0),
(40, 10, 'Юпитер', '', 1),
(41, 10, 'Венера', '', 0),
(42, 11, 'Совмещение Юпитера и Меркурия', '', 0),
(43, 11, 'Соотношение Луны и Венеры в каждой карте', '', 0),
(44, 11, 'Соотношение Солнца и Луны', '', 0),
(45, 11, 'Соотношение Лун в двух картах', '', 1),
(46, 12, 'Нахождение Меркурия в 7 доме от Арудха Лагны (АЛ)', '', 0),
(47, 12, 'Экзальтированный Меркурий, Юпитер или Венера во 2м доме от АЛ', '', 1),
(48, 12, 'Марс в знаках Меркурия в 11 доме от АЛ', '', 0),
(49, 12, 'Юпитер в Козероге в 7 доме от АЛ', '', 0),
(50, 13, 'Юпитер в Козероге в 7 доме от АЛ', '', 1),
(51, 14, 'Индивидуальная (Заочно)', '', 1),
(52, 14, 'Групповая (Очно)', '', 1),
(53, 1, '1', '', 1),
(54, 1, '2', '', 1),
(55, 1, '3', '', 1),
(56, 1, '4', '', 1),
(57, 1, '5', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `_quiz_questions_old`
--

DROP TABLE IF EXISTS `_quiz_questions_old`;
CREATE TABLE `_quiz_questions_old` (
  `id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) UNSIGNED NOT NULL,
  `question` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `orders` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_quiz_questions_old`
--

INSERT INTO `_quiz_questions_old` (`id`, `quiz_id`, `question`, `question_type`, `orders`) VALUES
(1, 1, 'Сколько лет Вы изучаете Астрологию', 'rating', 1),
(2, 1, 'Какие курсы уже проходили', 'comment', 2),
(3, 1, 'В каком доме находится Асцендент?', 'checkbox', 3),
(4, 1, 'В каком доме находится созвездие Рыб?', 'checkbox', 4),
(5, 1, 'Представители какого знака зодиака наиболее эмоциональны, сентиментальны и наделены интуицией?', 'checkbox', 5),
(6, 1, 'В каких домах снижается негативное действие планеты во второй половине жизни?', 'checkbox', 6),
(7, 1, 'Какие караки четвертого дома?', 'checkbox', 7),
(8, 1, 'Какими тремя Накшатрами владеет Луна', 'checkbox', 8),
(9, 1, 'Какие задачи ставит перед человеком АтмаКарака Сатурн?', 'checkbox', 9),
(10, 1, 'Какой подпериод будет вторым в Махадаше Юпитера?', 'checkbox', 10),
(11, 1, 'За эмоциональную и психологическую совместимость партнеров отвечает', 'checkbox', 11),
(12, 1, 'Какая комбинация может сделать натива богатым', 'checkbox', 12),
(13, 1, 'Что измениться в Вашей жизни после изучения Астрологии?', 'comment', 13),
(14, 1, 'Какая форма изучения, в настоящий момент, Вам удобнее?', 'radiogroup', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `_quiz_user_answers_old`
--

DROP TABLE IF EXISTS `_quiz_user_answers_old`;
CREATE TABLE `_quiz_user_answers_old` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `question_id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) UNSIGNED NOT NULL,
  `user_answer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `_quiz_user_answers_old`
--

INSERT INTO `_quiz_user_answers_old` (`user_id`, `question_id`, `quiz_id`, `user_answer`, `created_at`, `updated_at`) VALUES
(2, 3, 1, '7', NULL, NULL),
(2, 2, 1, 'tyuytu', NULL, NULL),
(2, 1, 1, '56', NULL, NULL),
(2, 3, 1, '10', NULL, NULL),
(3, 1, 1, '55', NULL, NULL),
(3, 2, 1, 'werwer', NULL, NULL),
(3, 3, 1, '8', NULL, NULL),
(3, 3, 1, '9', NULL, NULL),
(3, 3, 1, '7', NULL, NULL),
(3, 4, 1, '14', NULL, NULL),
(3, 4, 1, '12', NULL, NULL),
(3, 4, 1, '11', NULL, NULL),
(3, 5, 1, '17', NULL, NULL),
(3, 5, 1, '19', NULL, NULL),
(3, 6, 1, '22', NULL, NULL),
(3, 6, 1, '20', NULL, NULL),
(3, 7, 1, '27', NULL, NULL),
(3, 7, 1, '26', NULL, NULL),
(3, 8, 1, '33', NULL, NULL),
(3, 8, 1, '31', NULL, NULL),
(3, 9, 1, '37', NULL, NULL),
(3, 10, 1, '39', NULL, NULL),
(3, 11, 1, '45', NULL, NULL),
(3, 12, 1, '48', NULL, NULL),
(3, 13, 1, 'werwerwe', NULL, NULL),
(3, 14, 1, '51', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`),
  ADD KEY `articles_category_id_foreign` (`category_id`),
  ADD KEY `articles_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `article_comments`
--
ALTER TABLE `article_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_comments_article_id_foreign` (`article_id`),
  ADD KEY `article_comments_comment_author_foreign` (`comment_author`);

--
-- Индексы таблицы `article_likes`
--
ALTER TABLE `article_likes`
  ADD UNIQUE KEY `article_likes_article_id_user_id_unique` (`article_id`,`user_id`),
  ADD KEY `article_likes_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `article_tags`
--
ALTER TABLE `article_tags`
  ADD UNIQUE KEY `article_tags_article_id_tags_id_unique` (`article_id`,`tags_id`),
  ADD KEY `article_tags_tags_id_foreign` (`tags_id`);

--
-- Индексы таблицы `author_follow`
--
ALTER TABLE `author_follow`
  ADD UNIQUE KEY `author_follow_author_id_user_id_unique` (`author_id`,`user_id`),
  ADD KEY `author_follow_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Индексы таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Индексы таблицы `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Индексы таблицы `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_category_id_foreign` (`category_id`),
  ADD KEY `quiz_curator_id_foreign` (`curator_id`);

--
-- Индексы таблицы `quiz_answers`
--
ALTER TABLE `quiz_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_quiz_answers_users` (`user_id`),
  ADD KEY `FK_quiz_answers_quiz` (`quiz_id`);

--
-- Индексы таблицы `quiz_contacts`
--
ALTER TABLE `quiz_contacts`
  ADD PRIMARY KEY (`quiz_id`,`user_id`),
  ADD KEY `FK_quiz_contacts_users_2` (`user_id`);

--
-- Индексы таблицы `quiz_tags`
--
ALTER TABLE `quiz_tags`
  ADD PRIMARY KEY (`quiz_id`,`tag_id`),
  ADD KEY `FK_quiz_tags_tags` (`tag_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `seo_params`
--
ALTER TABLE `seo_params`
  ADD PRIMARY KEY (`id`,`page`);

--
-- Индексы таблицы `system_params`
--
ALTER TABLE `system_params`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topics_category_id_foreign` (`category_id`),
  ADD KEY `FK_topics_users` (`user_id`);

--
-- Индексы таблицы `topic_comments`
--
ALTER TABLE `topic_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topic_comments_topic_id_foreign` (`topic_id`),
  ADD KEY `FK_topic_comments_users` (`user_id`);

--
-- Индексы таблицы `topic_comment_likes`
--
ALTER TABLE `topic_comment_likes`
  ADD UNIQUE KEY `topic_comment_id_user_id` (`topic_comment_id`,`user_id`),
  ADD KEY `FK_topic_comment_likes_users` (`user_id`);

--
-- Индексы таблицы `topic_followers`
--
ALTER TABLE `topic_followers`
  ADD UNIQUE KEY `topic_id_user_id` (`topic_id`,`user_id`),
  ADD KEY `FK_topic_followers_users` (`user_id`);

--
-- Индексы таблицы `topic_likes`
--
ALTER TABLE `topic_likes`
  ADD UNIQUE KEY `topic_id_user_id` (`topic_id`,`user_id`),
  ADD KEY `FK_topic_likes_users` (`user_id`);

--
-- Индексы таблицы `topic_tags`
--
ALTER TABLE `topic_tags`
  ADD PRIMARY KEY (`topic_id`,`tag_id`),
  ADD KEY `fk_topic_tags_to_topics` (`topic_id`),
  ADD KEY `fk_topic_tags_to_tags` (`tag_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_position_id_foreign` (`position_id`);

--
-- Индексы таблицы `user_positions`
--
ALTER TABLE `user_positions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `_quiz_answers_old`
--
ALTER TABLE `_quiz_answers_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_answers_question_id_foreign` (`question_id`);

--
-- Индексы таблицы `_quiz_questions_old`
--
ALTER TABLE `_quiz_questions_old`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_questions_quiz_id_foreign` (`quiz_id`);

--
-- Индексы таблицы `_quiz_user_answers_old`
--
ALTER TABLE `_quiz_user_answers_old`
  ADD KEY `quiz_user_answers_user_id_foreign` (`user_id`),
  ADD KEY `quiz_user_answers_quiz_id_foreign` (`quiz_id`),
  ADD KEY `quiz_user_answers_question_id_foreign` (`question_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `article_comments`
--
ALTER TABLE `article_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=528;

--
-- AUTO_INCREMENT для таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `quiz_answers`
--
ALTER TABLE `quiz_answers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `system_params`
--
ALTER TABLE `system_params`
  MODIFY `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT для таблицы `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблицы `topic_comments`
--
ALTER TABLE `topic_comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `user_positions`
--
ALTER TABLE `user_positions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `_quiz_answers_old`
--
ALTER TABLE `_quiz_answers_old`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT для таблицы `_quiz_questions_old`
--
ALTER TABLE `_quiz_questions_old`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `article_comments`
--
ALTER TABLE `article_comments`
  ADD CONSTRAINT `article_comments_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_comments_comment_author_foreign` FOREIGN KEY (`comment_author`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `article_likes`
--
ALTER TABLE `article_likes`
  ADD CONSTRAINT `article_likes_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `article_tags`
--
ALTER TABLE `article_tags`
  ADD CONSTRAINT `article_tags_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_tags_tags_id_foreign` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `author_follow`
--
ALTER TABLE `author_follow`
  ADD CONSTRAINT `author_follow_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `author_follow_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `quiz_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_curator_id_foreign` FOREIGN KEY (`curator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `quiz_answers`
--
ALTER TABLE `quiz_answers`
  ADD CONSTRAINT `FK_quiz_answers_quiz` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`),
  ADD CONSTRAINT `FK_quiz_answers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `quiz_contacts`
--
ALTER TABLE `quiz_contacts`
  ADD CONSTRAINT `FK_quiz_contacts_users` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`),
  ADD CONSTRAINT `FK_quiz_contacts_users_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `quiz_tags`
--
ALTER TABLE `quiz_tags`
  ADD CONSTRAINT `FK_quiz_tags_quiz` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_quiz_tags_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `topics`
--
ALTER TABLE `topics`
  ADD CONSTRAINT `FK_topics_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `topics_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `topic_comments`
--
ALTER TABLE `topic_comments`
  ADD CONSTRAINT `FK_topic_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `topic_comments_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `topic_comment_likes`
--
ALTER TABLE `topic_comment_likes`
  ADD CONSTRAINT `FK_topic_comment_likes_topic_comments` FOREIGN KEY (`topic_comment_id`) REFERENCES `topic_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_topic_comment_likes_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `topic_followers`
--
ALTER TABLE `topic_followers`
  ADD CONSTRAINT `FK_topic_followers_topics` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`),
  ADD CONSTRAINT `FK_topic_followers_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `topic_likes`
--
ALTER TABLE `topic_likes`
  ADD CONSTRAINT `FK_topic_likes_topics` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`),
  ADD CONSTRAINT `FK_topic_likes_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `topic_tags`
--
ALTER TABLE `topic_tags`
  ADD CONSTRAINT `topic_tags_to_tag` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`),
  ADD CONSTRAINT `topic_tags_to_topic` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `user_positions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `_quiz_answers_old`
--
ALTER TABLE `_quiz_answers_old`
  ADD CONSTRAINT `_quiz_answers_old_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `_quiz_questions_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `_quiz_questions_old`
--
ALTER TABLE `_quiz_questions_old`
  ADD CONSTRAINT `quiz_questions_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `_quiz_user_answers_old`
--
ALTER TABLE `_quiz_user_answers_old`
  ADD CONSTRAINT `quiz_user_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `_quiz_questions_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_user_answers_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_user_answers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
